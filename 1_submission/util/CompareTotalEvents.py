import pyAMI.atlas.api as atlasAPI
import pyAMI.client
import rucio.client
import glob
import sys

def get_dataset_list(fname):
  dataset_list = []
  with open(fname) as f:
    for line in f.readlines():
      if line[0] == '#' or line[0] == ' ' or line[0] == '\n':
        continue # skip comments and empty lines
      else:
        dataset_list.append(line.strip())
  return dataset_list

if __name__ == '__main__':
  if sys.argv[-1] == __file__:
    print 'execute via \"python', __file__, 'path/to/file_containing_samples.txt\"'
    raise SystemExit

  ami_client = pyAMI.client.Client('atlas')
  atlasAPI.init()

  rucio_client = rucio.client.Client()

  datasets = get_dataset_list(sys.argv[-1])

  for dataset in datasets:

    version_string = ('_').join(dataset.split('.')[-1].split('_')[0:-1]) #remove p-tag

    AMI_DAOD = atlasAPI.get_dataset_info(ami_client, dataset)

    if len(AMI_DAOD) > 1:
      print 'multiple matches? not sure how to deal with this'

    AMI_sum_DAOD = int(AMI_DAOD[0][u'totalEvents'])

    prov = atlasAPI.get_dataset_prov(ami_client, dataset)

    for ds in prov[u'node']:
      if ds[u'dataType'] != u'AOD':
        continue # want to compare to AOD only

      elif ds[u'logicalDatasetName'].split('.')[-1] != version_string:
        continue # tags do not match

      else:
        print ' # comparing AOD and TOPQ1 DAOD:'
        print ' #', dataset
        print ' #', ds[u'logicalDatasetName']

        AMI_sum_AOD  = int(ds[u'events'])

        print ' # evts in  AOD   (AMI)', AMI_sum_AOD
        print ' # evts in DAOD   (AMI)', AMI_sum_DAOD

        scope = dataset.split('.')[0]

        rucio_result_AOD  = rucio_client.list_files(scope, ds[u'logicalDatasetName'])
        rucio_result_DAOD = rucio_client.list_files(scope, dataset)

        rucio_sum_AOD  = 0
        rucio_sum_DAOD = 0

        for r in rucio_result_AOD:  rucio_sum_AOD  += r[u'events']
        for r in rucio_result_DAOD: rucio_sum_DAOD += r[u'events']

        print ' # evts in  AOD (rucio)', rucio_sum_AOD
        print ' # evts in DAOD (rucio)', rucio_sum_DAOD

        if (rucio_sum_AOD != AMI_sum_AOD) or (rucio_sum_DAOD != AMI_sum_DAOD):
          continue
          print ' # # # # # # # #  rucio / AMI mismatch! # # # # # # # #'
          print dataset

        elif (rucio_sum_AOD != rucio_sum_DAOD):
          print ' # # # # # # # # AOD and DAOD disagree! # # # # # # # #'
          print dataset
          print 'AOD:', rucio_sum_AOD, 'DAOD:', rucio_sum_DAOD

        print ''
