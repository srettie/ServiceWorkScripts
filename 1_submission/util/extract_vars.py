import ROOT
import sys

if __name__ == '__main__':

  f = ROOT.TFile.Open(sys.argv[-1])
  leaves_n = [l.GetName() for l in f.Get('nominal').GetListOfLeaves()] # name
  leaves_t = [l.GetTypeName() for l in f.Get('nominal').GetListOfLeaves()] # type
  f.Close()

  weights = [l for l in leaves_n if l[0:6] == 'weight']
  weights.sort()
  print ' ################## weights ################## '
  for w in weights:
    print w

  leaves_n_sorted = leaves_n[:]
  leaves_n_sorted.sort()
  print ' ################## variables ################## '
  for v in leaves_n_sorted:
    if v[0:6] != 'weight':
      print v
