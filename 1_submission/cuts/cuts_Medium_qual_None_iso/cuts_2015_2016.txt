LibraryNames libTopEventSelectionTools libTopEventReconstructionTools

### Good Run List
GRLDir GoodRunsLists
GRLFile data15_13TeV/20170619/physics_25ns_21.0.19.xml data16_13TeV/20180129/physics_25ns_21.0.19.xml # 2015+2016

### Pile-up reweighting tool - this is now mandatory
PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.FS.v2/prw.merged.root
PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.AF.v2/prw.merged.root
PRWCustomScaleFactor 1.0/1.03:1.0/0.99:1.0/1.07
PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root

ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMTopoJets
LargeJetCollectionName None
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName None

TruthCollectionName None
TruthJetCollectionName None

ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::EventSaverFlatNtuple
OutputEvents SelectedEvents
OutputFilename output.root
PerfStats No

#Systematics MUON_ID__1down,MUON_ID__1up,MUON_MS__1down,MUON_MS__1up,MUON_SAGITTA_RESBIAS__1down,MUON_SAGITTA_RESBIAS__1up,MUON_SAGITTA_RHO__1down,MUON_SAGITTA_RHO__1up,MUON_SCALE__1down,MUON_SCALE__1up
Systematics Nominal
JetUncertainties_NPModel CategoryReduction
JetUncertainties_BunchSpacing 25ns


MuonQuality Medium
MuonQualityLoose Medium
MuonIsolation None
MuonIsolationLoose None


# DoTight/DoLoose to activate the loose and tight trees
# each should be one in: Data, MC, Both, False
DoTight Both
DoLoose False

# Turn on MetaData to pull IsAFII from metadata
UseAodMetaData True
#IsAFII False # should not be needed any more

BTaggingWP MV2c10:FixedCutBEff_60 MV2c10:FixedCutBEff_70 MV2c10:FixedCutBEff_77 MV2c10:FixedCutBEff_85

#NEvents 100

########################
### basic selection with mandatory cuts for reco level
########################

SUB BASIC
INITIAL
GRL
GOODCALO
PRIVTX
RECO_LEVEL

########################
### definition of the data periods
########################

SUB period_2015
RUN_NUMBER >= 276262
RUN_NUMBER <= 284484

SUB period_2016
RUN_NUMBER >= 297730
RUN_NUMBER <= 311481

SUB period_2017
RUN_NUMBER >= 325713
RUN_NUMBER <= 340453

SUB period_2018
RUN_NUMBER >= 348885

########################
### definition of tag trigggers
########################

SUB TAG_2016
. period_2016
TRIGDEC HLT_xe100_L1XE60

SUB TAG_2017
. period_2017
TRIGDEC HLT_xe110_pufit_L1XE55
# note: HLT_xe110_pufit_L1XE50 is pre-scaled in some runs!

SUB TAG_2018
. period_2018
TRIGDEC HLT_xe110_pufit_xe70_L1XE50

########################
### definition of event topologies (jets and muons)
########################

SUB WJETS
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 <= 4
JET_N_BTAG FixedCutBEff_77 < 1
MU_N 27000 >= 1
MU_N 27000 == 1
EL_N 27000 == 0
NOBADMUON

SUB TTBAR
JETCLEAN LooseBad
JET_N 25000 >= 4
JET_N_BTAG FixedCutBEff_77 >= 1
MU_N 27000 >= 1
MU_N 27000 == 1
EL_N 27000 == 0
NOBADMUON

########################
### tag selection (only tagged events will be saved)
########################

SELECTION tag_wjets_2016
. BASIC       # basic reqs
. TAG_2016    # select data-taking year, tag trigger
. WJETS       # topology: jet reqs, ==1 mu && ==0 el
SAVE

SELECTION tag_wjets_2017
. BASIC
. TAG_2017
. WJETS
SAVE

SELECTION tag_wjets_2018
. BASIC
. TAG_2018
. WJETS
SAVE

SELECTION tag_ttbar_2016
. BASIC
. TAG_2016
. TTBAR
SAVE

SELECTION tag_ttbar_2017
. BASIC
. TAG_2017
. TTBAR
SAVE

SELECTION tag_ttbar_2018
. BASIC
. TAG_2018
. TTBAR
SAVE

########################
### probe trigger selection (producing bool flags)
########################

# HLT_mu50 for 2016
SELECTION probe_HLT_mu50_2016
. BASIC
. period_2016
TRIGDEC HLT_mu50
TRIGMATCH

# HLT_mu50 for 2017
SELECTION probe_HLT_mu50_2017
. BASIC
. period_2017
TRIGDEC HLT_mu50
TRIGMATCH

# HLT_mu50 for 2018
SELECTION probe_HLT_mu50_2018
. BASIC
. period_2018
TRIGDEC HLT_mu50
TRIGMATCH

# HLT_mu26_ivarmedium for 2016
SELECTION probe_HLT_mu26_ivarmedium_2016
. BASIC
. period_2016
TRIGDEC HLT_mu26_ivarmedium
TRIGMATCH

# HLT_mu26_ivarmedium for 2017
SELECTION probe_HLT_mu26_ivarmedium_2017
. BASIC
. period_2017
TRIGDEC HLT_mu26_ivarmedium
TRIGMATCH

# HLT_mu26_ivarmedium for 2018
SELECTION probe_HLT_mu26_ivarmedium_2018
. BASIC
. period_2018
TRIGDEC HLT_mu26_ivarmedium
TRIGMATCH
