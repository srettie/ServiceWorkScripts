import os

template_dir = "inputs/"

muon_qual_list = ["Medium", "HighPt"]
muon_iso_list  = ["None", "FCTightTrackOnly", "FCTight"]

def extract_lines_from_file(template_directory, fname):
  with open(template_directory + fname) as f:
    lines = f.readlines()
  return lines

def create_muon_segment(muon_qual, muon_iso):
  lines = []
  lines.append("MuonQuality " + muon_qual + "\n")
  lines.append("MuonQualityLoose " + muon_qual + "\n")
  lines.append("MuonIsolation " + muon_iso + "\n")
  lines.append("MuonIsolationLoose " + muon_iso + "\n")
  if muon_iso == "FCTightTrackOnly":
    lines.append("MuonIsolationSF FixedCutHighMuTrackOnly\n")
    lines.append("MuonIsolationSFLoose FixedCutHighMuTrackOnly\n")
  elif muon_iso == "FCTight":
    lines.append("MuonIsolationSF FixedCutHighMuTight\n")
    lines.append("MuonIsolationSFLoose FixedCutHighMuTight\n")
  lines.append("\n")
  lines.append("\n")
  return lines

def write_all_to_file(fname, list_of_segments, folder_info):
  folder_name = "cuts_" + folder_info[0] + "_qual_" + folder_info[1] + "_iso"
  if not os.path.exists(folder_name):
    os.makedirs(folder_name)

  with open(folder_name + "/" + fname, "w+") as f:
    for segment in list_of_segments:
      for line in segment:
        f.write(line)


if __name__ == "__main__":
  print " # creating cut files from templates in", template_dir
  print " # muon quality settings:"
  for setting in muon_qual_list:
    print "  -", setting
  print " # muon isolation settings:"
  for setting in muon_iso_list:
     print "  -", setting

  # read all lines from templates
  head       = extract_lines_from_file(template_dir, "0_head.txt")

  GRL_20156  = extract_lines_from_file(template_dir, "1_GRL_20156.txt")
  GRL_2017   = extract_lines_from_file(template_dir, "1_GRL_2017.txt")
  GRL_2018   = extract_lines_from_file(template_dir, "1_GRL_2018.txt")

  PRW_16a = extract_lines_from_file(template_dir, "2_PRW_16a.txt")
  PRW_16d = extract_lines_from_file(template_dir, "2_PRW_16d.txt")
  PRW_16e = extract_lines_from_file(template_dir, "2_PRW_16e.txt")

  lumicalc_20156 = extract_lines_from_file(template_dir, "3_lumicalc_20156.txt")
  lumicalc_2017  = extract_lines_from_file(template_dir, "3_lumicalc_2017.txt") 
  lumicalc_2018  = extract_lines_from_file(template_dir, "3_lumicalc_2018.txt") 

  objects    = extract_lines_from_file(template_dir, "4_objects.txt")

  misc       = extract_lines_from_file(template_dir, "6_misc.txt")

  selection  = extract_lines_from_file(template_dir, "7_selection.txt")

  for muon_iso in muon_iso_list:
    for muon_qual in muon_qual_list:
      muons      = create_muon_segment(muon_qual, muon_iso)

      write_all_to_file("cuts_2015_2016.txt", [head, GRL_20156, PRW_16a, lumicalc_20156, objects, muons, misc, selection], [muon_qual, muon_iso])
      write_all_to_file("cuts_2017.txt", [head, GRL_2017, PRW_16d, lumicalc_2017, objects, muons, misc, selection], [muon_qual, muon_iso])
      write_all_to_file("cuts_2018.txt", [head, GRL_2018, PRW_16e, lumicalc_2018, objects, muons, misc, selection], [muon_qual, muon_iso])

  print " # successfully created cuts files"
