setupATLAS
mkdir athena build run
cd athena
asetup AnalysisTop,21.2.59,here
cd ../build
cmake ../athena
cmake --build ./
source */setup.sh
cd ..
