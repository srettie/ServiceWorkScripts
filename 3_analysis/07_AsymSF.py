import ROOT
import sys
import os
import codecs
import shlex
import glob
#import AtlasStyle
from array import array #for custom bin width

import random

#import save_to_spreadsheet as sts


ROOT.gROOT.SetBatch(ROOT.kTRUE)
ROOT.TH1.SetDefaultSumw2()
ROOT.gStyle.SetOptStat(0)

ROOT.gStyle.SetHatchesLineWidth(2)
ROOT.gStyle.SetEndErrorSize(0);


#ROOT.gROOT.LoadMacro("atlasstyle/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("atlasstyle/AtlasUtils.C")
ROOT.gROOT.LoadMacro("atlasstyle/AtlasLabels.C")
#ROOT.SetAtlasStyle()

ROOT.gStyle.SetPalette(112) # 2d viridis

#ROOT.gStyle.SetHatchesSpacing(2)
#ROOT.gROOT.LoadMacro("/home/alheld/atlasstyle-00-03-05/AtlasStyle.C")
#ROOT.gROOT.SetAtlasStyle()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

b_op = '77' # TODO change back!
njets = '>=4' #sys.argv[-3] #'==4'
nbtags = '>=1' #sys.argv[-2] #'==2'
splitttbar = False 
trigger_list = ['HLT_xe100_L1XE60', 'HLT_mu50', 'HLT_mu26_ivarmedium']
trigger_2015 = [] 
trigger_2016 = ['HLT_xe100_L1XE60', 'HLT_mu26_ivarmedium', 'HLT_mu50']
trigger = ''

doClosure = False
onlyHighpT = False # require mu_pT > 100 GeV for data/mc plots 

doUncEtaPhi = False

interactive = True # interactive script submission by default
script_options = sys.argv[1:]
if len(script_options) == 2:
  interactive = False
  

if interactive:
  for itrigger, trigger_choice in enumerate(trigger_list):
    print ' ', itrigger, '-', trigger_choice

  try:
    choice = int(raw_input('select trigger: '))
    trigger = trigger_list[choice]
  except:
    print 'not implemented'

else:
  trigger = trigger_list[int(script_options[0])]

print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

met_cut = '*(met_met > 200000)' # nominal: 200 GeV MET cut
jet_pt_cut = ''                 # nominal: jet pT > 25 GeV (default ATop)
file_folder = 'files_nominal'   # nominal: events saved in folder 'nominal'
tree_to_use = 'nominal'         # nominal: events are in the 'nominal' tree


# missing: isolation
systematics_settings = ['nominal', 'b_tag_70', 'met_150', 'jet_pt_30', 'no_iso', 'tight_qual', 'MUON_ID__1down', 'MUON_ID__1up', 'MUON_MS__1down', 'MUON_MS__1up', 'MUON_SCALE__1down', 'MUON_SCALE__1up']

if interactive: 
  for i, syst_set in enumerate(systematics_settings):
    print ' ', i, '-', syst_set

  user_in = raw_input('select systematic setting: ')

else:
  user_in =  script_options[1]

if user_in == '0':
  pass                                                # using default settings
elif user_in == '1':
  b_op = '70'                                         # mv2c10 A 77% -> 70%
elif user_in == '2':
  met_cut = '*(met_met > 150000)'                     # MET > 200 GeV -> 150 GeV
elif user_in == '3':
  jet_pt_cut = '*(Sum$(jet_pt<=30000) == 0)'          # jet pT > 25 GeV -> 30 GeV
elif user_in == '4':
  file_folder = 'files_noiso'                         # muon isolation Gradient -> None
elif user_in == '5':
  file_folder = 'files_tight'                         # muon quality Medium -> Tight

elif user_in == '6':
  tree_to_use = 'MUON_ID__1down'
elif user_in == '7':
  tree_to_use = 'MUON_ID__1up'
elif user_in == '8':
  tree_to_use = 'MUON_MS__1down'
elif user_in == '9':
  tree_to_use = 'MUON_MS__1up'
elif user_in == '10':
  tree_to_use = 'MUON_SCALE__1down'
elif user_in == '11':
  tree_to_use = 'MUON_SCALE__1up'

systematics_name_spreadsheet = systematics_settings[int(user_in)]

print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
print trigger
print systematics_name_spreadsheet, b_op, met_cut
print 'files from', './' + file_folder + '/' 
print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

# used for efficiency plots
#TagTrig = 'HLT_xe70'#, 'HLT_xe100_mht_L1XE50'
ProbeTriggers = [
                 #'Probe_HLT_mu26_ivarmedium',
                 'HLT_mu26_ivarmedium',
                 #'Probe_HLT_mu50',
                 'HLT_mu50',
                ]

print njets, nbtags

raw_yield = False 

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

user = 'alheld.'

data_identifier = 'data'
plot_folder = './'  + 'plots_' + systematics_name_spreadsheet + '/'
download_folder = './' + file_folder + '/'

EventWeight = '(weight_mc)*(weight_pileup)*(weight_leptonSF)*(weight_bTagSF_' + b_op + ')*(weight_jvt)*(weight_norm)/weight_indiv_SF_MU_Trigger'
#EventWeight = '(weight_mc)*(weight_pileup)*(weight_leptonSF)*(weight_bTagSF_' + b_op + ')*(weight_jvt)*(weight_norm)'
print 'REMOVE LEPTON SF!?'
#EventWeight = '(weight_mc)*(weight_pileup)*(weight_bTagSF_' + b_op + ')*(weight_jvt)*(weight_norm)'
if raw_yield:
  EventWeight = '(1==1)'

lumi = 32861.6 / 1000.0 #/fb #33257.2 / 1000.0 #/fb 2016, http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/GoodRunsLists/data16_13TeV/20161101/notes.txt 
lumi16 = lumi
print lumi, '/fb'

doMerging = False

mcSamples  = glob.glob(download_folder + 'singletop/full.root')
mcSamples += glob.glob(download_folder + 'ttbar/full.root')
mcSamples += glob.glob(download_folder + 'ttH/full.root')
mcSamples += glob.glob(download_folder + 'wjets/full.root')
mcSamples += glob.glob(download_folder + 'zjets/full.root')
mcSamples += glob.glob(download_folder + 'ttV/full.root')
mcSamples += glob.glob(download_folder + 'diboson/full.root')

mcSamples.sort()
#print mcSamples
print len(mcSamples), 'mc samples'

dataSamples = glob.glob(download_folder + 'data/full_data.root')
#dataSamples = glob.glob(download_folder + 'data_2015/full_data.root')
#dataSamples = glob.glob(download_folder + 'data_2016/full_data.root')
dataSamples.sort()
print len(dataSamples), 'data samples'


def GetN(sampleName):
    # Open file and get first entry in cutflow
    #myfile = ROOT.TFile.Open(sampleName+'output.root', 'OPEN')
    myfile = ROOT.TFile.Open(sampleName, 'OPEN')
    cutflowHist = myfile.Get('nominal/cutflow_mc_pu_zvtx') #TODO how to extract!?
    N = cutflowHist.GetBinContent(1)
    return N

def GetXS(sampleName):
    sampleNumber = sampleName[sampleName.find(user)+len(user)+11:sampleName.find(user)+len(user)+6+11]
    isPowheg = sampleName.find('Powheg') > 0

    #print sampleName
    #print sampleNumber

    #TODO why is this needed?
    #if isPowheg:
    #XSFile = codecs.open(', 'r').read().splitlines() #TODO #TODO #TODO CHECK WHICH ONE TO USE
    XSFile = codecs.open(xsec_file, 'r').read().splitlines()
    #else:
    #    print 'not implemented! add file'
    #    XSFile = codecs.open('/global/alheld/servicetask/XSections_13TeV_e3651_e4133.txt', 'r').read().splitlines()
    
    for line in XSFile:
        if len(line) == 0 or not line[0].isdigit():
            continue
        buff = shlex.split(line, '\t')
        if str(buff[0]) != sampleNumber:
            continue
        if str(buff[0]) == sampleNumber:
            # Return XS, kFactor, and filterEff
            #print 'XS is '+str(buff[1])
            #print 'kFactor is '+str(buff[2])
            #print 'filterEff is '+str(buff[3])
  
            # TODO edited!
            #if isPowheg:
            return float(buff[1]), float(buff[2]), 1.0
            #else:
            #    return float(buff[1]), float(buff[2]), float(buff[3])
    print "Sample number not found! Returning 0..."
    return 0

def GetHisto(sampleName, varexp, selection, emptyHisto, HF=''):
    #print 'HF =', HF

    isData = data_identifier in sampleName
    #print isData
    # Initial Histogram Setup
    sampleNumber = sampleName.split('/')[-2]
    #print sampleNumber
    #setupFile = ROOT.TFile.Open(sampleName+'output.root', 'read')
    setupFile = ROOT.TFile.Open(sampleName, 'read')
    # Get tree from file
    if not isData:
      readTree = setupFile.Get(tree_to_use)
    else:
      readTree = setupFile.Get('nominal')
    # Clone empty histogram for desired binning
    #if isData:
    #    sampleNumber = sampleName[sampleName.find(user)+len(user)+11:sampleName.find(user)+len(user)+11+8]
    #else:
    #    sampleNumber = sampleName[sampleName.find(user)+len(user)+11:sampleName.find(user)+len(user)+11+6]
    fullHisto = emptyHisto.Clone( sampleNumber )
    fullHisto.Sumw2()
    tempCanvas = ROOT.TCanvas()
    tempCanvas.cd()

    if True: #('410000' not in sampleName) or (410120 not in tt.ttbar): # remove overlap from 410000 if processing 410120 too
      if b_op == '77':
        b_mv2c10_val = 0.645925 # mv2c10!
      elif b_op == '70':
        b_mv2c10_val = 0.8244273 # MV2C10 now! 
      elif b_op == '60':
        b_mv2c10_val = 0.934906

      #b_tagging_cut = '(1==1)' #'(Sum$(jet_mv2c10>=' + str(b_mv2c10_val) + ')' + nbtags + ')'
      if b_op != '77':
        b_tagging_cut = '(Sum$(jet_mv2c10>=' + str(b_mv2c10_val) + ')' + nbtags + ')'
      else:
        b_tagging_cut = '(1==1)'
      #print b_tagging_cut

      trigger_selection = '*(' + trigger + '==1)'
      selection += trigger_selection

      selection += met_cut
      selection += jet_pt_cut

      #selection += '*(abs(mu_eta) <= 2.4)' # trigger acceptance range only up to |eta| <= 2.4

      if varexp == 'mu' and isData:
        print ' ~~ scaling data mu by 1.0/1.09' #1.16'
        varexp += '*1.0/1.09' #1.16'

      #if isData:
      #  selection += '*(runNumber>=303943)*(runNumber<=304494)' # period C
      #else:
      #  selection += '*(randomRunNumber>=303943)*(randomRunNumber<=304494)' # period C

      #if not isData:
      #  selection += '*((Probe_HLT_mu50==1) ? 0.1 : 1)' 
      #  #selection += '*(0.5)' 

      if onlyHighpT:
        selection += '*(mu_pt > 100000)'

      if doClosure:
        if 'HLT_mu26_ivarmedium' in selection and not isData:
          if 'mu_eta > -1.05' in selection:
            print 'applying barrel SF'
            selection += '*(0.9176)'
            #selection += '*(weight_indiv_SF_MU_Trigger)'
          if 'mu_eta <= -1.05' in selection:
            print 'applying endcap SF'
            selection += '*(0.9711)'
            #selection += '*(weight_indiv_SF_MU_Trigger)'
          #print varexp, selection

      #print 'selection', selection

      # standard
      if HF=='':
        if trigger in trigger_2015:
          #print ' ~ 2015 trigger selected, vetoing 2016 events', sampleNumber
          selection += '* (randomRunNumber >= 276262)*(randomRunNumber <= 284484)'

        if trigger in trigger_2016: 
          #print ' ~ 2016 trigger selected, vetoing 2015 events', sampleNumber
          selection += '* (randomRunNumber >= 297730)'

        #print varexp+'>>'+sampleNumber
        #print b_tagging_cut + ' * ' + selection
        #print readTree
        readTree.Draw( varexp+'>>'+sampleNumber, b_tagging_cut + ' * ' + selection)


      elif HF=='2015':
        if trigger in trigger_2016:
          #print ' ~ 2016 trigger selected, vetoing 2015 events (HF)', sampleNumber
          selection += '* (0==1)'
        readTree.Draw( varexp+'>>'+sampleNumber, b_tagging_cut + ' * ' + selection + '* (runNumber < 290000)')

      elif HF=='2016':
        if trigger in trigger_2015: 
          #print ' ~ 2015 trigger selected, vetoing 2016 events (HF)', sampleNumber
          selection += '* (0==1)'
        #print varexp+'>>'+sampleNumber
        #print b_tagging_cut + ' * ' + selection + '* (runNumber > 290000)'
        readTree.Draw( varexp+'>>'+sampleNumber, b_tagging_cut + ' * ' + selection + '* (runNumber > 290000)')

    try:
      if region[-7] == '(':
        region = 'all'
    except:
      pass

    fullHisto.SetDirectory(0)
    setupFile.Close()
    return fullHisto

def CombineCutStrings(ListOfCutStrings, Operator):
    NumStrings = len(ListOfCutStrings)
    ReturnCutString = ''
    for entry in range( NumStrings ):
        ReturnCutString += '(' + ListOfCutStrings[entry] + ')'
        if entry < NumStrings-1:
            ReturnCutString += ' '+Operator+' '
    return ReturnCutString

def ExtractPtCutFromTriggerName(TriggerName):
    TriggerName.lower
    PtCut = TriggerName[TriggerName.find('_mu')+3:]    
    if PtCut.find('_')<0:
        return float(PtCut)
    else:
        return float(PtCut[:PtCut.find('_')])

def CreateCutFlow(samples):
    can = ROOT.TCanvas()
    can.cd()
    cutflow = ROOT.TH1D()
    first = True
    isData = data_identifier in samples[0]
    for sample in samples:
        # Open file and get cutflow
        #myfile = ROOT.TFile.Open(sample+'output.root', 'OPEN')
        myfile = ROOT.TFile.Open(sample, 'OPEN')
        #
        #cutflowTemp = myfile.Get('WPlusJetsSelection_Tag_'+TagTrig[4:]+'/cutflow_mc_pu_zvtx')
        #
        cutflowTemp = myfile.Get('Tag_'+'/cutflow_mc_pu_zvtx')
#probably need just Tag_
        if not isData:
            #print GetN(sample)
            XS, kFactor, filterEff = GetXS(sample)
            cutflowTemp.Scale( (XS*kFactor*filterEff) / GetN(sample) ) #TODO TODO TODO TODO CHANGE BACK
        if first == True:
            cutflow = cutflowTemp
            first = False
        else:
            cutflow.Add( cutflow, cutflowTemp )
        cutflow.SetDirectory(0)
        myfile.Close()
    
    cutflow.Draw('text')
    if isData:
        can.Print(plot_folder + 'TagTrigger_'+'/cutflow_data.pdf')
    else:
        can.Print(plot_folder + 'TagTrigger_'+'/cutflow.pdf')

def CreateValidationPlots(mcSamples, dataSamples, EventWeight, Luminosity):
    can = ROOT.TCanvas("c1", "canvas", 1300, 1600)

    nBinsPt = 20
    minPt      =   0
    maxPt      = 400
        
    nBinsEta =  24
    minEta     = -2.5
    maxEta      =  2.5
    
    nBinsPhi =  24
    minPhi     = -3.15
    maxPhi      =  3.15

    var_options = [
                     ['jet_pt', nBinsPt, minPt, maxPt, 'GeV'],
                     ['jet_eta', nBinsEta, minEta, maxEta, ''],
                     ['jet_phi', nBinsPhi, minPhi, maxPhi, ''],
                     ['mu_pt', nBinsPt, minPt, maxPt, 'GeV'],
                     #['mu_eta', nBinsEta, minEta, maxEta, ''],
                     ['mu_eta', 15, -2.5, 2.5, '', [-2.5, -2.0, -1.6, -1.3, -1, -0.7, -0.4, -0.1, 0.1, 0.4, 0.7, 1, 1.3, 1.6, 2.0, 2.5]], 
                     #['mu_phi', nBinsPhi, minPhi, maxPhi, ''],
                     ['mu_phi', 17, -3.16, 3.16, '', [-3.16, -2.905, -2.59, -2.12, -1.805, -1.335, -1.02, -0.55, -0.235, 0.235, 0.55, 1.02, 1.335, 1.805, 2.12, 2.59, 2.905, 3.16]], #MCP binning
                     ['met_met', 20, 160, 560, 'GeV'],
                     ['mu', 50, 0, 50, ''],
                     ['weight_indiv_SF_MU_Trigger', 50, 0.3, 1.3, ''],
                     #['met_met', 1, 0, 5000, 'GeV'],
                  ] 

    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    for ivar, var in enumerate(var_options):
      print ivar, var 

    try:
      choice = int(raw_input('select variable, press enter to plot all: '))
      myVars = [var_options[choice]]  
    except:
      # plot all
      myVars = var_options

    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'


    for var in myVars:
        pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0) 
        #pad1.SetBottomMargin(0.08)
        pad1.SetBottomMargin(0.05)
        pad1.SetGridx()
        pad1.Draw()
        pad1.cd()

        #pad1.SetLogy()
        #raise SystemExit

        print '\n# Creating plot for ' + var[0]
        # Create MC and Data plots

        custom_binning = False
        #if 'mu_eta' in var[0] or 'mu_phi' in var[0]:
        #  custom_binning = True
  
        if custom_binning:
          nbins = var[1]
          cust_bin = var[-1]

          emptyHist = ROOT.TH1D('emptyHist'+var[0], '', nbins, array('d', cust_bin))
  
          ttH_plot = ROOT.TH1D('ttH_hist'+var[0], '', nbins, array('d', cust_bin)) 
          ttH_plot.Sumw2()
  
          ttbar_light_plot = ROOT.TH1D('ttbar_light_hist'+var[0], '', nbins, array('d', cust_bin))
          ttbar_light_plot.Sumw2()
          
          ttbar_c_plot = ROOT.TH1D('ttbar_c_hist'+var[0], '', nbins, array('d', cust_bin))
          ttbar_c_plot.Sumw2()
  
          ttbar_b_plot = ROOT.TH1D('ttbar_b_hist'+var[0], '', nbins, array('d', cust_bin))
          ttbar_b_plot.Sumw2()
  
          stop_plot = ROOT.TH1D('single_top_hist'+var[0], '', nbins, array('d', cust_bin))
          stop_plot.Sumw2()
  
          wjets_plot = ROOT.TH1D('wjets_sh_hist'+var[0], '', nbins, array('d', cust_bin))
          wjets_plot.Sumw2()
  
          zjets_plot = ROOT.TH1D('zjets_hist'+var[0], '', nbins, array('d', cust_bin))
          zjets_plot.Sumw2()
  
          ttV_plot = ROOT.TH1D('ttV_hist'+var[0], '', nbins, array('d', cust_bin))
          ttV_plot.Sumw2() 
  
          diboson_plot = ROOT.TH1D('diboson_hist'+var[0], '', nbins, array('d', cust_bin))
          diboson_plot.Sumw2() 
  
          mcPlot = ROOT.THStack('MCHist'+var[0], '')
  
          mcSum = ROOT.TH1D('mcSum'+var[0], '', nbins, array('d', cust_bin))
          mcSum.Sumw2()
  
          #mcDiv = ROOT.TH1D('mcDiv'+var[0], '', nbins, array('d', cust_bin))
          #mcDiv.Sumw2()
  
          data15Plot = ROOT.TH1D('Data15Hist'+var[0], '', nbins, array('d', cust_bin))
          data15Plot.Sumw2()

          data16Plot = ROOT.TH1D('Data16Hist'+var[0], '', nbins, array('d', cust_bin))
          data16Plot.Sumw2()
 
        else:
          emptyHist = ROOT.TH1D('emptyHist'+var[0], '', var[1], var[2], var[3])
  
          ttH_plot = ROOT.TH1D('ttH_hist'+var[0], '', var[1], var[2], var[3])
          ttH_plot.Sumw2()
  
          ttbar_light_plot = ROOT.TH1D('ttbar_light_hist'+var[0], '', var[1], var[2], var[3])
          ttbar_light_plot.Sumw2()
          
          ttbar_c_plot = ROOT.TH1D('ttbar_c_hist'+var[0], '', var[1], var[2], var[3])
          ttbar_c_plot.Sumw2()
  
          ttbar_b_plot = ROOT.TH1D('ttbar_b_hist'+var[0], '', var[1], var[2], var[3])
          ttbar_b_plot.Sumw2()
  
          stop_plot = ROOT.TH1D('single_top_hist'+var[0], '', var[1], var[2], var[3])
          stop_plot.Sumw2()
  
          wjets_plot = ROOT.TH1D('wjets_sh_hist'+var[0], '', var[1], var[2], var[3])
          wjets_plot.Sumw2()
  
          zjets_plot = ROOT.TH1D('zjets_hist'+var[0], '', var[1], var[2], var[3])
          zjets_plot.Sumw2()
  
          ttV_plot = ROOT.TH1D('ttV_hist'+var[0], '', var[1], var[2], var[3])
          ttV_plot.Sumw2() 
  
          diboson_plot = ROOT.TH1D('diboson_hist'+var[0], '', var[1], var[2], var[3])
          diboson_plot.Sumw2() 
  
          mcPlot = ROOT.THStack('MCHist'+var[0], '')
  
          mcSum = ROOT.TH1D('mcSum'+var[0], '', var[1], var[2], var[3])
          mcSum.Sumw2()
  
          #mcDiv = ROOT.TH1D('mcDiv'+var[0], '', var[1], var[2], var[3])
          #mcDiv.Sumw2()

          data15Plot = ROOT.TH1D('Data15Hist'+var[0], '', var[1], var[2], var[3])
          data15Plot.Sumw2()

          data16Plot = ROOT.TH1D('Data16Hist'+var[0], '', var[1], var[2], var[3]) 
          data16Plot.Sumw2()

        for mcSample in mcSamples:
            if '_pt' in var[0] or 'met' in var[0] or 'HT' in var[0]:
                if ('ttH' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttH]):
                    print 'adding', mcSample, 'to ttH'
                    ttH_plot.Add( ttH_plot, GetHisto(mcSample, var[0]+'/1000', '(' + EventWeight + ')', emptyHist) )

                #if ('ttbar' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttbar]):
                #    print 'adding', mcSample, 'to ttbar light'
                #    ttbar_light_plot.Add( ttbar_light_plot, GetHisto(mcSample, var[0]+'/1000', '(' + EventWeight + ')', emptyHist, HF='light') )

                #if ('ttbar' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttbar]):
                #    print 'adding', mcSample, 'to ttbar c'
                #    ttbar_c_plot.Add( ttbar_c_plot, GetHisto(mcSample, var[0]+'/1000', '(' + EventWeight + ')', emptyHist, HF='c') )

                if ('ttbar' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttbar]):
                    print 'adding', mcSample, 'to ttbar (no flavor splitting)'
                    ttbar_b_plot.Add( ttbar_b_plot, GetHisto(mcSample, var[0]+'/1000', '(' + EventWeight + ')', emptyHist, HF='') )

                if ('singletop' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.singletop]):
                    print 'adding', mcSample, 'to single top'
                    stop_plot.Add( stop_plot, GetHisto(mcSample, var[0]+'/1000', '(' + EventWeight + ')', emptyHist) )

                if ('wjets' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.wjets]):
                   print 'adding', mcSample, 'to wjets'
                   wjets_plot.Add( wjets_plot, GetHisto(mcSample, var[0]+'/1000', '(' + EventWeight + ')', emptyHist) )

                if ('zjets' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.zjets]):
                    print 'adding', mcSample, 'to zjets'
                    zjets_plot.Add( zjets_plot, GetHisto(mcSample, var[0]+'/1000', '(' + EventWeight + ')', emptyHist) )

                if ('ttV' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttV_tZ]):
                    print 'adding', mcSample, 'to ttV_tZ'
                    ttV_plot.Add( ttV_plot, GetHisto(mcSample, var[0]+'/1000', '(' + EventWeight + ')', emptyHist) )

                if ('diboson' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.diboson]):
                    print 'adding', mcSample, 'to diboson'
                    diboson_plot.Add( diboson_plot, GetHisto(mcSample, var[0]+'/1000', '(' + EventWeight + ')', emptyHist) )
            else:
                if ('ttH' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttH]):
                    print 'adding', mcSample, 'to ttH'
                    ttH_plot.Add( ttH_plot, GetHisto(mcSample, var[0], '(' + EventWeight + ')', emptyHist) )

                #if ('ttbar' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttbar]):
                #    print 'adding', mcSample, 'to ttbar light'
                #    ttbar_light_plot.Add( ttbar_light_plot, GetHisto(mcSample, var[0], '(' + EventWeight + ')', emptyHist, HF='light') )

                #if ('ttbar' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttbar]):
                #    print 'adding', mcSample, 'to ttbar c'
                #    ttbar_c_plot.Add( ttbar_c_plot, GetHisto(mcSample, var[0], '(' + EventWeight + ')', emptyHist, HF='c') )

                if ('ttbar' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttbar]):
                    print 'adding', mcSample, 'to ttbar (no flavor splitting)'
                    ttbar_b_plot.Add( ttbar_b_plot, GetHisto(mcSample, var[0], '(' + EventWeight + ')', emptyHist, HF='') )

                if ('singletop' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.singletop]):
                    print 'adding', mcSample, 'to single top'
                    stop_plot.Add( stop_plot, GetHisto(mcSample, var[0], '(' + EventWeight + ')', emptyHist) )

                if ('wjets' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.wjets]):
                    print 'adding', mcSample, 'to wjets Sherpa'
                    wjets_plot.Add( wjets_plot, GetHisto(mcSample, var[0], '(' + EventWeight + ')', emptyHist) )

                if ('zjets' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.zjets]):
                    print 'adding', mcSample, 'to zjets'
                    zjets_plot.Add( zjets_plot, GetHisto(mcSample, var[0], '(' + EventWeight + ')', emptyHist) )

                if ('ttV' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.ttV_tZ]):
                    print 'adding', mcSample, 'to ttV_tZ'
                    ttV_plot.Add( ttV_plot, GetHisto(mcSample, var[0], '(' + EventWeight + ')', emptyHist) )

                if ('diboson' in mcSample):
                #if any([str(sample) in mcSample for sample in tt.diboson]):
                    print 'adding', mcSample, 'to diboson'
                    diboson_plot.Add( diboson_plot, GetHisto(mcSample, var[0], '(' + EventWeight + ')', emptyHist) )

        for dataSample in dataSamples:
            if '_pt' in var[0] or 'met' in var[0] or 'HT' in var[0]:
                print 'adding', dataSample, 'to data 2015'
                data15Plot.Add( data15Plot, GetHisto(dataSample, var[0]+'/1000', '1', emptyHist, HF='2015') )
                print 'adding', dataSample, 'to data 2016'
                data16Plot.Add( data16Plot, GetHisto(dataSample, var[0]+'/1000', '1', emptyHist, HF='2016') )
            else:
                print 'adding', dataSample, 'to data 2015'
                data15Plot.Add( data15Plot, GetHisto(dataSample, var[0], '1', emptyHist, HF='2015') )
                print 'adding', dataSample, 'to data 2016'
                data16Plot.Add( data16Plot, GetHisto(dataSample, var[0], '1', emptyHist, HF='2016') )

        if 'TMath' in var[0]:
          var[0] = 'MEM_D2 (alpha = ' + str(alpha) + ')'

        # Set Background Colors and line colors
        ttH_plot.SetFillColor(ROOT.kGreen+2)
        ttH_plot.SetLineColor(ROOT.kGreen+2)
        #ttH_plot.SetMarkerColor(ROOT.kGreen+2)
        ttbar_light_plot.SetFillColor(ROOT.kOrange+8)
        ttbar_light_plot.SetLineColor(ROOT.kOrange+8)
        #ttbar_light_plot.SetMarkerColor(ROOT.kOrange+8)
        ttbar_c_plot.SetFillColor(ROOT.kRed-3)
        ttbar_c_plot.SetLineColor(ROOT.kRed-3)
        #ttbar_c_plot.SetMarkerColor(ROOT.kRed-3)
        ttbar_b_plot.SetFillColor(ROOT.kRed+1)      # NEW
        ttbar_b_plot.SetLineColor(ROOT.kRed+1)
        #ttbar_b_plot.SetMarkerColor(ROOT.kRed+2)

        if splitttbar != True:
          ttbar_b_plot.Add(ttbar_b_plot, ttbar_c_plot)
          ttbar_b_plot.Add(ttbar_b_plot, ttbar_light_plot)
          ttbar_b_plot.SetFillColor(ROOT.kRed-3)
          ttbar_b_plot.SetLineColor(ROOT.kRed-3)
          #ttbar_b_plot.SetMarkerColor(ROOT.kRed-3)

        stop_plot.SetFillColor(ROOT.kSpring)   # NEW
        stop_plot.SetLineColor(ROOT.kSpring)
        #stop_plot.SetLineWidth(2)

        #wjets_plot.SetFillColorAlpha(0,1)
        wjets_plot.SetFillColor(ROOT.kAzure-9)   # NEW
        wjets_plot.SetLineColor(ROOT.kAzure-9)
        #wjets_plot.SetMarkerColor(ROOT.kOrange+2)
        #wjets_plot.SetLineWidth(2)

        zjets_plot.SetFillColor(ROOT.kOrange)    # NEW
        zjets_plot.SetLineColor(ROOT.kOrange)
        #zjets_plot.SetMarkerColor(ROOT.kCyan)
        ttV_plot.SetFillColor(ROOT.kCyan)    # NEW
        ttV_plot.SetLineColor(ROOT.kCyan)
        #ttV_plot.SetMarkerColor(ROOT.kYellow)
        diboson_plot.SetFillColor(ROOT.kYellow)  # NEW
        diboson_plot.SetLineColor(ROOT.kYellow)
        #diboson_plot.SetMarkerColor(ROOT.kMagenta+2)

        # Scale MC to luminosity
        # Note that XS used are given in /pb, so multiply by 1000 to convert to /fb
  
        # optional: normalize shapes to 1
        sg_norm = 1#/19.7410504832
        bg_norm = 1#/523.20926121  
        # can also use in the 6j3b case to multiply by 10:
        #if different_region == '_6j3b':
        #  wjets_plot.Scale(10)
        #  zjets_plot.Scale(10)
        #  ttV_plot.Scale(10)

        if not raw_yield:
          ttH_plot.Scale(1000*Luminosity*sg_norm)
          ttbar_light_plot.Scale(1000*Luminosity*bg_norm)
          ttbar_c_plot.Scale(1000*Luminosity*bg_norm)
          ttbar_b_plot.Scale(1000*Luminosity*bg_norm) 
          stop_plot.Scale(1000*Luminosity*bg_norm)
          wjets_plot.Scale(1000*Luminosity*bg_norm)
          zjets_plot.Scale(1000*Luminosity*bg_norm)
          ttV_plot.Scale(1000*Luminosity*bg_norm)
          diboson_plot.Scale(1000*Luminosity*bg_norm)

        sig = [ttH_plot.Integral()]
        bg = [ttbar_light_plot.Integral(), ttbar_c_plot.Integral(), ttbar_b_plot.Integral(), stop_plot.Integral(), wjets_plot.Integral(), zjets_plot.Integral(), ttV_plot.Integral(), diboson_plot.Integral()]

        print 'ttH', sig[0]
        if splitttbar == True: 
          print 'ttbar light', bg[0] 
          print 'ttbar c', bg[1] 
          print 'ttbar b', bg[2] 
        else:
          print 'ttbar', bg[2]
        print 'stop', bg[3]
        print 'wjets Sherpa', bg[4]
        #print 'wjets MadGraph', bg_mg
        print 'zjets', bg[5]
        print 'ttV', bg[6] 
        print 'diboson', bg[7] 
        print 'sig sum', sum(sig)
        print 'bg sum', sum(bg), bg
        try:
          print 'ratio', sum(sig)/sum(bg)
        except:
          pass
        interror = ROOT.Double()
        if sum(sig)/sum(bg) < 1: #0.05: #0.02:
          print 'data 2015', data15Plot.IntegralAndError(0, data15Plot.GetNbinsX(), interror), interror
          print 'data 2016', data16Plot.IntegralAndError(0, data16Plot.GetNbinsX(), interror), interror
          try:
            print 'data 2016 / 2015 ratio:', data16Plot.IntegralAndError(0, data16Plot.GetNbinsX(), interror) / data15Plot.IntegralAndError(0, data15Plot.GetNbinsX(), interror)
          except:
            print 'cannot determine 2016 / 2015 data ratio'
        else:
          print 'data blinded, S/B =', sum(sig)/sum(bg)

        
        dataPlot = data15Plot.Clone()
        dataPlot.Sumw2()
        dataPlot.Add(dataPlot, data16Plot)

        data_mc_ratio = dataPlot.IntegralAndError(0, dataPlot.GetNbinsX(), interror) / (sum(sig)+sum(bg)) 
        print 'data/mc:', data_mc_ratio
    
        # sum all MC but w+jets in one histogram
        mcSum.Add(mcSum, ttH_plot)
        if splitttbar == True:
          mcSum.Add(mcSum, ttbar_light_plot)
          mcSum.Add(mcSum, ttbar_c_plot)
        mcSum.Add(mcSum, ttbar_b_plot)
        mcSum.Add(mcSum, stop_plot) 
        mcSum.Add(mcSum, zjets_plot)
        mcSum.Add(mcSum, ttV_plot)
        mcSum.Add(mcSum, diboson_plot)
        mcSum.Add(mcSum, wjets_plot)

        # add in different w options
        #mcSum_n = mcSum.Clone()

        #mcSum.SetLineWidth(2)
        #mcSum.SetLineColor(ROOT.kBlue)
        #mcSum.SetMarkerColorAlpha(ROOT.kBlue,1)
        #mcSum.SetMarkerStyle(ROOT.kFullCircle)
        #mcSum.SetMarkerSize(0) #circle can be scaled

        mcSum.SetFillColor(ROOT.kGray+2)
        mcSum.SetLineColor(ROOT.kGray+2)
        mcSum.SetFillStyle(3354)

        # Add MC to THStack
        mcPlot.Add(ttV_plot)
        mcPlot.Add(diboson_plot)
        mcPlot.Add(stop_plot)
        mcPlot.Add(zjets_plot)

        mcPlot.Add(wjets_plot)
        mcPlot.Add(ttbar_b_plot)  # biggest contribution last
        if splitttbar == True:
          mcPlot.Add(ttbar_c_plot)  # biggest contribution last
          mcPlot.Add(ttbar_light_plot)  # biggest contribution last
        mcPlot.Add(ttH_plot)


        # different stacks of both generators
        #mcPlot_n = mcPlot.Clone()

        pad1.cd()

        # Draw MC and data on same plot
        #mcPlot_sh.Draw("Fhist")#Fhist")# TEXT")
        #mcPlot_mg.Draw("PO SAME")# TEXT")
        #mcPlot.Draw("Fhist SAME")

        mcPlot.Draw("Fhist")

        mcSum.Draw("SAME E2")

        #mcPlot_sh.Draw("P0 HIST SAME")#Fhist")# TEXT")
        #mcPlot_mg.Draw("PO HIST SAME")# TEXT")

        #mcPlot_sh.Draw("AXIS SAME") #workaround to show axis again
        #mcPlot.Draw("E3")
        mcPlot.SetMinimum(0)
        mcPlot.SetMaximum(1.65*dataPlot.GetMaximum())
        if dataPlot.GetMaximum() == 0:
          mcPlot.SetMaximum(1.3*mcSum.GetMaximum())
        #mcPlot.GetXaxis().SetTitle(str(var[0]) + ' ' +  str(var[3]))
        if var[4]!='':
          mcPlot.GetYaxis().SetTitle('Events / ' + str((var[3]-var[2])/var[1]) + ' ' + str(var[4]))
        else:
          mcPlot.GetYaxis().SetTitle('Events')
        mcPlot.GetYaxis().SetTitleOffset(1.5)

        #mcPlot.GetYaxis().SetTitleSize(30)
        #mcPlot.GetYaxis().SetTitleFont(43)
        #mcPlot.GetYaxis().SetTitleOffset(2.5)
        #mcPlot.GetXaxis().SetLabelSize(25)
        #mcPlot.GetYaxis().SetLabelSize(10)

        dataPlot.SetLineColor(ROOT.kBlack)
        dataPlot.SetMarkerStyle(ROOT.kFullCircle)
        dataPlot.SetMarkerSize(2)
        dataPlot.SetLineWidth(2)
        #dataPlot.Draw("SAME E0")
        dataPlot.Draw("SAME E")

        #dataPlot.SetMarkerStyle(ROOT.kFullCircle) # different marker seems to be needed to scale to 0 properly so that it is invisible
        #mcSum.SetMarkerSize(0)
        #mcSum_sh.SetMarkerColor(ROOT.kOrange+2) # TODO how to get rid of this?
        #mcSum_mg.SetMarkerColor(ROOT.kOrange-2) # TODO how to get rid of this?

        # Add legend
        leg = ROOT.TLegend(0.41, 0.565, 0.89, 0.825)
        #leg.SetTextAlign(11)

        #interror_data = ROOT.Double()

        integral = round(dataPlot.IntegralAndError(0, dataPlot.GetNbinsX(), interror), 2)
        #leg.AddEntry(dataPlot,         'data 15             ' + str(round(dataPlot.Integral(),2)), 'lep')
        leg.AddEntry(dataPlot,         'data 2016      ' + str(integral) + ' +/- ' + str(round(interror,2)), 'lep')

        if splitttbar == True:
          integral = round(ttbar_light_plot.IntegralAndError(0, ttbar_light_plot.GetNbinsX(), interror), 2)
          #leg.AddEntry(ttbar_light_plot, 'tt + light            ' + str(round(bg[0],2)), 'F')
          leg.AddEntry(ttbar_light_plot, 'tt + light         ' + str(integral) + ' +/- ' + str(round(interror,2)), 'F')

          integral = round(ttbar_c_plot.IntegralAndError(0, ttbar_c_plot.GetNbinsX(), interror), 2)
          #leg.AddEntry(ttbar_c_plot,     'tt + cc               ' + str(round(bg[1],2)), 'F')
          leg.AddEntry(ttbar_c_plot,     'tt + cc            ' + str(integral) + ' +/- ' + str(round(interror,2)), 'F')

          integral = round(ttbar_b_plot.IntegralAndError(0, ttbar_b_plot.GetNbinsX(), interror), 2)
          #leg.AddEntry(ttbar_b_plot,     'tt + bb               ' + str(round(bg[2],2)), 'F')
          leg.AddEntry(ttbar_b_plot,     'tt + bb            ' + str(integral) + ' +/- ' + str(round(interror,2)), 'F')
        else:
          integral = round(ttbar_b_plot.IntegralAndError(0, ttbar_b_plot.GetNbinsX(), interror), 2)
          #leg.AddEntry(ttbar_b_plot,     'tt                       ' + str(round(bg[2],2)), 'F')
          leg.AddEntry(ttbar_b_plot,     'tt                    ' + str(integral) + ' +/- ' + str(round(interror,2)), 'F')

        integral = round(wjets_plot.IntegralAndError(0, wjets_plot.GetNbinsX(), interror), 2)
        #leg.AddEntry(wjets_plot,       'W Sherpa        ' + str(round(bg[4],2)), 'F')
        leg.AddEntry(wjets_plot,       'W + jets         ' + str(integral) + ' +/- ' + str(round(interror,2)), 'F')

        integral = round(zjets_plot.IntegralAndError(0, zjets_plot.GetNbinsX(), interror), 2)
        #leg.AddEntry(zjets_plot,       'Z + jets             ' + str(round(bg[5],2)), 'F')
        leg.AddEntry(zjets_plot,       'Z + jets          ' + str(integral) + ' +/- ' + str(round(interror,2)), 'F')

        integral = round(stop_plot.IntegralAndError(0, stop_plot.GetNbinsX(), interror), 2)
        #integral = round(stop_plot.IntegralAndError(0, stop_plot.GetNbinsX(), interror), 2)
        #leg.AddEntry(stop_plot,        'single top          ' + str(round(bg[3],2)), 'F')
        leg.AddEntry(stop_plot,        'single top       ' + str(integral) + ' +/- ' + str(round(interror,2)), 'F')

        integral = round(diboson_plot.IntegralAndError(0, diboson_plot.GetNbinsX(), interror), 2)
        #leg.AddEntry(diboson_plot,     'diboson             ' + str(round(bg[7],2)), 'F')
        leg.AddEntry(diboson_plot,     'diboson         ' + str(integral) + ' +/- ' + str(round(interror,2)), 'F')

        integral = round(ttV_plot.IntegralAndError(0, ttV_plot.GetNbinsX(), interror), 2)
        #leg.AddEntry(ttV_plot,         'ttV, tZ                ' + str(round(bg[6],2)), 'F')
        leg.AddEntry(ttV_plot,         'ttV, tZ             ' + str(integral) + ' +/- ' + str(round(interror,2)), 'F')

        leg.SetBorderSize(0)
        #leg.SetFillColor(0)
        leg.Draw("SAME")

        ROOT.ATLASLabel(0.15, 0.85, "Internal")
        latex = ROOT.TLatex()
        latex.SetNDC()
        latex.SetTextAlign(13)
        latex.SetTextSize(0.025)
        latex.DrawLatex(0.15, 0.82, 'L = ' + str(round(lumi,2)) + ' fb^{-1}')
        latex.DrawLatex(0.15, 0.79, njets + 'j, ' + nbtags + 'b')# @ ' + b_op + '%')
        latex.DrawLatex(0.15, 0.76, trigger)

        #latex.DrawLatex(0.15, 0.73, 'data / MC: ' + str(round(data_mc_ratio,4)))


        can.cd()
        pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
        pad2.SetTopMargin(0.05)
        pad2.SetBottomMargin(0.20)
        pad2.SetGridx()
        #pad2.Modified() # suddenly
        #pad2.Update()   # needed?
        pad2.SetFillStyle(4000)
        pad2.SetFillColor(0)
        pad2.SetFrameFillStyle(4000)
        pad2.Draw()
        pad2.cd()

        data_ratio_n = dataPlot.Clone()
        mc_ratio = mcSum.Clone()
        data_ratio_n.Divide(mc_ratio)

        data_ratio_n.SetLineColor(ROOT.kBlack)
        #data_ratio.SetLineWidth(10)
        #data_ratio.SetFillColor(ROOT.kGreen)

        # Get the ratio plot limits.
        #ratioPlotMinimum = data_ratio.GetBinContent(data_ratio.GetMinimumBin())
        #ratioPlotMaximum = data_ratio.GetBinContent(data_ratio.GetMaximumBin())
        #ratioPlotMinimum = mcPlot_sh.GetBinContent(data_ratio.GetMinimumBin())
        #ratioPlotMaximum = mcPlot_sh.GetBinContent(data_ratio.GetMaximumBin())
        ratioPlotLimitMinimum = 0.2 #abs(1 - 1.1 * ratioPlotMinimum)
        ratioPlotLimitMaximum = 0.2 #abs(1 - 1.1 * ratioPlotMaximum)
        ratioPlotLimit = max(ratioPlotLimitMinimum, ratioPlotLimitMaximum)
        data_ratio_n.SetMinimum(1 - ratioPlotLimitMinimum)
        data_ratio_n.SetMaximum(1 + ratioPlotLimitMaximum)
        data_ratio_n.SetMarkerStyle(ROOT.kFullCircle)

        # Set zeros to a value outside the visible range.
        for binNumber in range(1, data_ratio_n.GetNbinsX() + 1):
            if data_ratio_n.GetBinContent(binNumber) == 0:
                data_ratio_n.SetBinContent(binNumber, -21244344)
        # Create a histogram for error bars centred on unity.
        histogram4 = data_ratio_n.Clone()

        for binNumber in range(1, histogram4.GetNbinsX() + 1):
            histogram4.SetBinContent(binNumber, 1)
        histogram4.SetMarkerSize(0)

        if var[4]!='':
          histogram4.GetXaxis().SetTitle(str(var[0]) + ' (' + str(var[4]) + ')')
        else:
          histogram4.GetXaxis().SetTitle(str(var[0]))

        histogram4.GetYaxis().SetTitle('data / MC')
        histogram4.GetXaxis().SetTitleOffset(1.0)
        histogram4.GetYaxis().SetTitleOffset(0.525)
        histogram4.GetXaxis().SetTitleSize(0.1)
        histogram4.GetYaxis().SetTitleSize(0.1)
        histogram4.GetXaxis().SetLabelSize(0.10)
        histogram4.GetYaxis().SetLabelSize(0.10)
        #histogram4.GetYaxis().SetNdivisions(9)
        histogram4.Draw("AXIS") #AXIG") #E2") # switched errors over to data_ratio
        histogram5 = histogram4.Clone()
        histogram5.Draw("AXIG SAME") # workaround
        #data_ratio.Draw("SAME HIST E0")#P")
        data_ratio_n.SetMarkerStyle(ROOT.kFullCircle)
        data_ratio_n.SetLineColor(ROOT.kBlack)
        data_ratio_n.Draw("SAME E0")#P")
        #data_ratio.SetBarWidth(25)

        # TODO find out why uncommenting this makes titles and labels disappear
        #histogram4.GetYaxis().SetTitleSize(10)
        #histogram4.GetYaxis().SetTitleFont(43)
        #histogram4.GetYaxis().SetLabelFont(43)
        #histogram4.GetXaxis().SetTitleFont(43)
        #histogram4.GetXaxis().SetTitleOffset(4.)
        #histogram4.GetXaxis().SetLabelFont(43)
        #histogram4.GetXaxis().SetLabelSize(25)

        #mc_ratio.Draw("e2")
        #data_ratio.Draw("SAME")

        # Add a line for unity. 
        histogramAxisXMinimum = histogram4.GetXaxis().GetBinLowEdge(1)
        histogramAxisXMaximum = histogram4.GetXaxis().GetBinLowEdge(
            histogram4.GetSize() - 1
        )
        x1 = histogramAxisXMinimum
        y1 = 1
        x2 = histogramAxisXMaximum
        y2 = 1
        unityLine = ROOT.TLine(x1, y1, x2, y2)
        unityLine.Draw()
        #histogram4.GetXaxis().LabelsOption("v")


        # can.
        pad1.Modified()
        pad2.Modified()
        can.Modified()

     
        try:
          os.makedirs(plot_folder)
        except OSError:
          if not os.path.isdir(plot_folder):
            raise

        #print var[0]
 
        #pad1.Print(plot_folder + 'TagTrigger_'+'/Validation_'+var[0]+'.pdf')
        savestring = plot_folder + var[0] + '_ratio' #var[0]
        savestring = plot_folder + var[0] + '_ratio' #var[0]
        #print savestring

        if splitttbar == True:
          savestring += '_tt_split'

        if trigger != '1':
          savestring += '_' + trigger

        savestring += '.pdf'
        can.SaveAs(savestring)
        pad1.Clear()
        pad2.Clear()
        can.Clear()

 
def GetScaleFactors(mcSamples, dataSamples, ProbeTrig, Region, EventWeight):
    SF = 1.0

    return SF

# calculate relative stat. uncertainty per bin in a N bin distribution under the assumption of
# constant stat. unc. in each bin, with "N" bins reaching from "low" to "high" 
# TODO properly include N!=1 functionality
def GetRelUnc(inputRootFiles_mc, inputRootFiles_data, TagTrig, ProbeTrig, Region, EventWeight, N=1, low=100.0, high=600.0):
    inputRootFiles = inputRootFiles_mc + inputRootFiles_data
    if trigger in trigger_2015:
      year_selected = '2015'
    if trigger in trigger_2016:
      year_selected = '2016'

    plotvar = 'mu_pt/1000'
    NumBins = 1
    custom_bins = [ low, high]
    Bin_Min     = low
    Bin_Max     = high

    if Region=='barrel':
        CutString_EtaRange = 'mu_eta >= -1.05 && mu_eta <= 1.05'
    elif Region=='endcaps':
        CutString_EtaRange = 'mu_eta < -1.05 || mu_eta > 1.05'
    else:
        print 'ERROR: Region', Region, 'not found'
        sys.exit(1)

    CutString_tagOnly   = CombineCutStrings([ TagTrig,
                                              CutString_EtaRange
                                             ], '&&')

    if '2015' in ProbeTrig or '2016' in ProbeTrig:
      trigmatch_string = 'mu_trigMatch_'+('_').join(ProbeTrig.split('_')[1:-1]) # remove "Probe_" and "_201X"
    else:
      #trigmatch_string = 'mu_trigMatch_'+('_').join(ProbeTrig.split('_')[1:])   # remove "Probe_"
      #trigmatch_string = 'mu_trigMatch_'+('_').join(ProbeTrig.split('_')[0:])   # remove "Probe_"
      if '50' in ProbeTrig:
        trigmatch_string = 'mu_trigMatch_Probe_HLT_mu50'
      elif '26' in ProbeTrig:
        trigmatch_string = 'mu_trigMatch_HLT_mu26_ivarmedium'

    CutString_withProbe = CombineCutStrings([ TagTrig,
                                              ProbeTrig,
                                              trigmatch_string,
                                              CutString_EtaRange
                                             ], '&&')

    # Trigger efficiency as a function of muon pT
    emptyHist              = ROOT.TH1D('emptyHist'+str(random.random()),              str(random.random()), NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)

    Hist_pt_tagOnly_mc     = ROOT.TH1D('Hist_pt_tagOnly_mc'+str(random.random()),     str(random.random()), NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)
    Hist_pt_tagOnly_mc.Sumw2()
    Hist_pt_withProbe_mc   = ROOT.TH1D('Hist_pt_withProbe_mc'+str(random.random()),   str(random.random()), NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)
    Hist_pt_withProbe_mc.Sumw2()

    Hist_pt_tagOnly_data   = ROOT.TH1D('Hist_pt_tagOnly_data'+str(random.random()),   str(random.random()), NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)
    Hist_pt_tagOnly_data.Sumw2()
    Hist_pt_withProbe_data = ROOT.TH1D('Hist_pt_withProbe_data'+str(random.random()), str(random.random()), NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)
    Hist_pt_withProbe_data.Sumw2()

    Hist_efficiency_data = ROOT.TGraphAsymmErrors(emptyHist)
    Hist_efficiency_mc   = ROOT.TGraphAsymmErrors(emptyHist)


    Hist_SF = ROOT.TH1D('Hist_SF'+str(random.random()), 'Hist_SF'+str(random.random()), NumBins, array('d',custom_bins)) #ROOT.TGraphAsymmErrors(emptyHist)

    # Stich different pT slices together
    for rootFile in inputRootFiles:
	if 'data' in rootFile: 
	  if year_selected == '2015': 
	    Hist_pt_tagOnly_data.Add( Hist_pt_tagOnly_data, GetHisto(rootFile, plotvar, '(' + '1' + ')*(' + CutString_tagOnly   +')', emptyHist, HF='2015') )
	    Hist_pt_withProbe_data.Add( Hist_pt_withProbe_data, GetHisto(rootFile, plotvar, '(' + '1'+ ')*(' + CutString_withProbe   +')', emptyHist, HF='2015') )
	  if year_selected == '2016':
	    Hist_pt_tagOnly_data.Add( Hist_pt_tagOnly_data, GetHisto(rootFile, plotvar, '(' + '1'+ ')*(' + CutString_tagOnly   +')', emptyHist, HF='2016') )
	    Hist_pt_withProbe_data.Add( Hist_pt_withProbe_data, GetHisto(rootFile, plotvar, '(' + '1' + ')*(' + CutString_withProbe   +')', emptyHist, HF='2016') )

	else:
	  Hist_pt_tagOnly_mc.Add( Hist_pt_tagOnly_mc, GetHisto(rootFile, plotvar, '(' + EventWeight + ')*(' + CutString_tagOnly   +')', emptyHist) )
	  Hist_pt_withProbe_mc.Add( Hist_pt_withProbe_mc, GetHisto(rootFile, plotvar, '(' + EventWeight + ')*(' + CutString_withProbe   +')', emptyHist) )

    data_probe = Hist_pt_withProbe_data.Clone()
    data_tag   = Hist_pt_tagOnly_data.Clone()

    mc_probe   = Hist_pt_withProbe_mc.Clone()
    mc_tag     = Hist_pt_tagOnly_mc.Clone()
      
    data_probe.Divide(data_tag)
    mc_probe.Divide(mc_tag)

    Hist_efficiency_data.Divide(Hist_pt_withProbe_data, Hist_pt_tagOnly_data)
    Hist_efficiency_mc.Divide(Hist_pt_withProbe_mc, Hist_pt_tagOnly_mc)            # MC asym error divide - issue with weights

    Hist_SF.Divide(data_probe, mc_probe)   # default divide, errors adjusted below!

    xval_data = ROOT.Double()
    yval_data = ROOT.Double()
    xval_mc   = ROOT.Double()
    yval_mc   = ROOT.Double()

#      print '-- readjusting uncertainties in SF histogram, adding symmetrized uncertainties from efficiency histograms in quadrature' 
    total_rel_unc = 0 
    if 'pt' in plotvar:
      for binNumber in range(1, Hist_SF.GetNbinsX() + 1):
        #print 'error in symm bin ', Hist_SF.GetBinError(binNumber)
        Hist_efficiency_data.GetPoint(binNumber-1, xval_data, yval_data)
        Hist_efficiency_mc.GetPoint(binNumber-1, xval_mc, yval_mc)
        #print 'bin', binNumber, custom_bins[binNumber-1], xval_data, xval_mc
        try:
          asym_err = Hist_SF.GetBinContent(binNumber) * ( (Hist_efficiency_data.GetErrorY(binNumber-1) / yval_data)**2 + (Hist_efficiency_mc.GetErrorY(binNumber-1) / yval_mc)**2 )**0.5
        except:
          asym_err = 0.0
        total_rel_unc += (asym_err / Hist_SF.GetBinContent(binNumber))**2 
        Hist_SF.SetBinError(binNumber, asym_err)

    return  (N * total_rel_unc)**0.5


def CreateEfficiencyHistograms(inputRootFiles_mc, inputRootFiles_data, TagTrig, ProbeTrig, Region, EventWeight, doEtaPhi=False, plotvar='mu_pt', custom_binning = []):
    if inputRootFiles_mc != None and inputRootFiles_data != None:
      inputRootFiles = inputRootFiles_mc + inputRootFiles_data
    elif inputRootFiles_mc != None:
      inputRootFiles = inputRootFiles_mc
    else:
      inputRootFiles = inputRootFiles_data

    if trigger in trigger_2015:
      year_selected = '2015'
    if trigger in trigger_2016:
      year_selected = '2016'
    
    print '\n# Tag Trigger: '+str(TagTrig)
    print '# Probe Trigger: '+str(ProbeTrig)

    if plotvar == 'mu_pt':
      plotvar = 'mu_pt/1000'

      NumBins = 11
      #custom_bins = [ 0.0, 60.0, 80.0, 100.0, 120.0, 140.0, 160.0, 180.0, 200.0, 250.0, 300.0, 400.0, 600.0]
      custom_bins = [ 0.0, 60.0, 80.0, 100.0, 120.0, 140.0, 160.0, 180.0, 200.0, 250.0, 300.0, 600.0]

      if custom_binning != []:
        NumBins = 10
        custom_bins = custom_binning 

      Bin_Min     =   0.0
      Bin_Max     = 600.0
      Bin_Cutoff  = 100.0 # always fit only from 100 GeV onwards

    if plotvar == 'mu_eta':
      NumBins = 15
      custom_bins = [-2.5, -2.0, -1.6, -1.3, -1, -0.7, -0.4, -0.1, 0.1, 0.4, 0.7, 1, 1.3, 1.6, 2.0, 2.5]
      Bin_Min     =  -2.5
      Bin_Max     =   2.5
      Bin_Cutoff  =  -2.5 # fit full range

    if plotvar == 'mu_phi':
      NumBins = 17
      custom_bins = [-3.16, -2.905, -2.59, -2.12, -1.805, -1.335, -1.02, -0.55, -0.235, 0.235, 0.55, 1.02, 1.335, 1.805, 2.12, 2.59, 2.905, 3.16]
      Bin_Min     =  -3.16
      Bin_Max     =   3.16
      Bin_Cutoff  =  -3.16 # fit full range

    if plotvar == 'mu': # pileup
      #custom_bins = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0, 30.0, 31.0, 32.0, 33.0]
      custom_bins = [0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 22.0, 24.0, 26.0, 28.0, 30.0, 32.0]
      NumBins = 16
      Bin_Min     =  0.0 
      Bin_Max     = 32.0 
      Bin_Cutoff  =  0.0 # fit full range

    NumBinsInEta =  24
    if Region=='barrel':
	Eta_Min      = -1.05
	Eta_Max      =  1.05
    elif Region=='endcaps':
	Eta_Min      = -2.5  # region for endcap from run 1 paper is 1.05 < |eta| < 2.4 
	Eta_Max      =  2.5
	
    NumBinsInPhi =  24
    Phi_Min      = -3.15
    Phi_Max      =  3.15

    if Region=='barrel':
	CutString_EtaRange = 'mu_eta > -1.05 && mu_eta < 1.05'
    elif Region=='endcaps':
	CutString_EtaRange = 'mu_eta <= -1.05 || mu_eta >= 1.05'
    else:
	print 'ERROR: Region', Region, 'not found'
	sys.exit(1)
	
    CutString_tagOnly   = CombineCutStrings([ TagTrig, 
					      CutString_EtaRange
					     ], '&&')

    if '2015' in ProbeTrig or '2016' in ProbeTrig:
      trigmatch_string = 'mu_trigMatch_'+('_').join(ProbeTrig.split('_')[1:-1]) # remove "Probe_" and "_201X"
    else:
      #trigmatch_string = 'mu_trigMatch_'+('_').join(ProbeTrig.split('_')[1:])   # remove "Probe_"
      #trigmatch_string = 'mu_trigMatch_'+('_').join(ProbeTrig.split('_')[0:])   # remove "Probe_"
      if '50' in ProbeTrig:
        trigmatch_string = 'mu_trigMatch_HLT_mu50'
      elif '26' in ProbeTrig:
        trigmatch_string = 'mu_trigMatch_HLT_mu26_ivarmedium'

    CutString_withProbe = CombineCutStrings([ TagTrig, 
					      ProbeTrig, 
					      trigmatch_string, 
					      CutString_EtaRange
					     ], '&&')

    #print CutString_tagOnly
    #print CutString_withProbe    

    if 'pt' not in plotvar:
      if not os.path.exists(plot_folder + 'TagTrigger_'+TagTrig+'_'+plotvar):
  	os.makedirs(plot_folder + 'TagTrigger_'+TagTrig+'_'+plotvar)
    else:
      if not os.path.exists(plot_folder + 'TagTrigger_'+TagTrig+'_mu_pt'):
	os.makedirs(plot_folder + 'TagTrigger_'+TagTrig+'_mu_pt')

    if not doEtaPhi:
      #print ' ~~~~~~~~~ creating new canvas'
      #can = ROOT.TCanvas("c1" + ProbeTrig + year_selected + Region, "canvas" + ProbeTrig + year_selected + Region, 1300, 1600)
      can = ROOT.TCanvas("c1" + str(random.random()), "canvas"+str(random.random), 1300, 1600)
      #print 'can', "c1" + ProbeTrig + year_selected + Region
      if 'pt' in plotvar:
        padc = ROOT.TPad("padc"+str(random.random()), "padc"+str(random.random()), 0, 0.3, 1, 1.0) 
        padc.SetBottomMargin(0.05)
      else: 
        padc = ROOT.TPad("padc"+str(random.random()), "padc"+str(random.random()), 0, 0.05, 1, 1.0) 
        padc.SetBottomMargin(0.08)

      #padc.SetBottomMargin(0.08)
      #padc.SetBottomMargin(0.05)
      padc.SetGridx()
      padc.Draw()
      padc.cd()

      # Trigger efficiency as a function of muon pT
      emptyHist              = ROOT.TH1D('emptyHist',              '', NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)

      Hist_pt_tagOnly_mc     = ROOT.TH1D('Hist_pt_tagOnly_mc',     '', NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)
      Hist_pt_tagOnly_mc.Sumw2()
      Hist_pt_withProbe_mc   = ROOT.TH1D('Hist_pt_withProbe_mc',   '', NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)
      Hist_pt_withProbe_mc.Sumw2()

      Hist_pt_tagOnly_data   = ROOT.TH1D('Hist_pt_tagOnly_data',   '', NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)
      Hist_pt_tagOnly_data.Sumw2()
      Hist_pt_withProbe_data = ROOT.TH1D('Hist_pt_withProbe_data', '', NumBins, array('d',custom_bins)) #Bin_Min, Bin_Max)
      Hist_pt_withProbe_data.Sumw2()

      Hist_efficiency_data = ROOT.TGraphAsymmErrors(emptyHist)
      Hist_efficiency_mc   = ROOT.TGraphAsymmErrors(emptyHist)

      #Hist_efficiency_data = ROOT.TGraphAsymmErrors(emptyHist)
      #Hist_efficiency_mc   = ROOT.TGraphAsymmErrors(emptyHist)
#      print '-- warning! weighted events cause symmetric errors in ROOT.TGraphAsymmErrors'

      Hist_SF = ROOT.TH1D('Hist_SF'+str(random.random()), 'Hist_SF'+str(random.random()), NumBins, array('d',custom_bins)) #ROOT.TGraphAsymmErrors(emptyHist)
      #Hist_SF = ROOT.TH1D('Hist_SF', '', NumBins, array('d',custom_bins)) #ROOT.TGraphAsymmErrors(emptyHist)

      # Stich different pT slices together
      for rootFile in inputRootFiles:
	#print rootFile
	if 'data' in rootFile: 
	  if year_selected == '2015': 
	    Hist_pt_tagOnly_data.Add( Hist_pt_tagOnly_data, GetHisto(rootFile, plotvar, '(' + '1' + ')*(' + CutString_tagOnly   +')', emptyHist, HF='2015') )
	    Hist_pt_withProbe_data.Add( Hist_pt_withProbe_data, GetHisto(rootFile, plotvar, '(' + '1'+ ')*(' + CutString_withProbe   +')', emptyHist, HF='2015') )
	  if year_selected == '2016':
	    Hist_pt_tagOnly_data.Add( Hist_pt_tagOnly_data, GetHisto(rootFile, plotvar, '(' + '1'+ ')*(' + CutString_tagOnly   +')', emptyHist, HF='2016') )
	    Hist_pt_withProbe_data.Add( Hist_pt_withProbe_data, GetHisto(rootFile, plotvar, '(' + '1' + ')*(' + CutString_withProbe   +')', emptyHist, HF='2016') )

	else:
	  Hist_pt_tagOnly_mc.Add( Hist_pt_tagOnly_mc, GetHisto(rootFile, plotvar, '(' + EventWeight + ')*(' + CutString_tagOnly   +')', emptyHist) )
	  Hist_pt_withProbe_mc.Add( Hist_pt_withProbe_mc, GetHisto(rootFile, plotvar, '(' + EventWeight + ')*(' + CutString_withProbe   +')', emptyHist) )

      data_probe = Hist_pt_withProbe_data.Clone()
      data_tag   = Hist_pt_tagOnly_data.Clone()

      mc_probe   = Hist_pt_withProbe_mc.Clone()
      mc_tag     = Hist_pt_tagOnly_mc.Clone()
      
      data_probe.Divide(data_tag)
      mc_probe.Divide(mc_tag)

      Hist_efficiency_data.Divide(Hist_pt_withProbe_data, Hist_pt_tagOnly_data)
      Hist_efficiency_mc.Divide(Hist_pt_withProbe_mc, Hist_pt_tagOnly_mc)            # MC asym error divide - issue with weights

      # this needs to be done with asym errors    
      if 'pt' in plotvar:
        Hist_SF.Divide(data_probe, mc_probe)   # default divide, errors adjusted below!
        #Hist_SF.Divide(data_probe, mc_probe, 1, 1, "B") 
        #Hist_SF.BayesDivide(data_probe, mc_probe) only works on asym error graphs
      xval_data = ROOT.Double()
      yval_data = ROOT.Double()
      xval_mc   = ROOT.Double()
      yval_mc   = ROOT.Double()

#      print '-- readjusting uncertainties in SF histogram, adding symmetrized uncertainties from efficiency histograms in quadrature' 
      total_rel_unc = 0 
      if 'pt' in plotvar:
        for binNumber in range(1, Hist_SF.GetNbinsX() + 1):
          #print 'error in symm bin ', Hist_SF.GetBinError(binNumber)
          Hist_efficiency_data.GetPoint(binNumber-1, xval_data, yval_data)
          Hist_efficiency_mc.GetPoint(binNumber-1, xval_mc, yval_mc)
          #print 'bin', binNumber, custom_bins[binNumber-1], xval_data, xval_mc
          if Hist_SF.GetBinContent(binNumber) == 0:
            print '--- !!!' 
            print '--- !!!' 
            print '--- !!! WARNING: empty bin in SF calculation, will most likely cause issues - consider rebinning around', xval_mc, xval_data, 'GeV'
            print '--- !!!' 
            print '--- !!!' 
          #print Hist_SF.GetBinContent(binNumber), yval_data, yval_mc
          try:
            asym_err = Hist_SF.GetBinContent(binNumber) * ( (Hist_efficiency_data.GetErrorY(binNumber-1) / yval_data)**2 + (Hist_efficiency_mc.GetErrorY(binNumber-1) / yval_mc)**2 )**0.5
          except:
            asym_err = 0.0
          total_rel_unc += (asym_err / Hist_SF.GetBinContent(binNumber))**2 
          #print custom_bins[binNumber-1]
          #print 'combined from asym', asym_err 
          #print 'data error in asym', Hist_efficiency_data.GetErrorY(binNumber-1), Hist_efficiency_data.GetErrorYhigh(binNumber-1), Hist_efficiency_data.GetErrorYlow(binNumber-1)
          #print 'mc error in asym  ', Hist_efficiency_mc.GetErrorY(binNumber-1),   Hist_efficiency_mc.GetErrorYhigh(binNumber-1),   Hist_efficiency_mc.GetErrorYlow(binNumber-1)
          #print '----------------'
          Hist_SF.SetBinError(binNumber, asym_err)
      print 'tot. rel. unc:', 1 / float(NumBins) * (total_rel_unc)**0.5

      #import numpy as np
      #print np.array(Hist_SF)

      Hist_efficiency_mc.SetTitle(ProbeTrig+' ('+Region+')')
      #Hist_efficiency_mc.GetXaxis().SetTitle('muon p_{T} [GeV]')
      Hist_efficiency_mc.GetYaxis().SetTitle('efficiency')

      if 'pt' in plotvar:
        Hist_SF.SetMarkerStyle(ROOT.kFullCircle)
        Hist_SF.SetMarkerSize(0)

      Hist_efficiency_mc.SetLineColor(ROOT.kGreen+2)
      Hist_efficiency_mc.SetFillColorAlpha(ROOT.kGreen+1, 0.5)
      Hist_efficiency_mc.SetFillStyle(3244)
      #Hist_efficiency_mc.SetLineWidth(2)

      Hist_efficiency_data.SetLineColor(ROOT.kGray+3)
      Hist_efficiency_data.SetFillColorAlpha(ROOT.kGray+2, 0.5)
      Hist_efficiency_data.SetFillStyle(3244)
      #Hist_efficiency_data.SetLineWidth(2)

      if 'pt' in plotvar:
        Hist_SF.SetLineColor(ROOT.kBlue+1)
        Hist_SF.SetFillColorAlpha(ROOT.kBlue, 0.5)
        Hist_SF.SetFillStyle(3244)
      #Fit = ROOT.TF1('pol0','pol0', Bin_Cutoff, Bin_Max)
      Fit_data = ROOT.TF1('pol1','[0]+[1]*x', Bin_Cutoff, Bin_Max)
      Fit_c_data = ROOT.TF1('pol0','[0]', Bin_Cutoff, Bin_Max)
      Fit_data.SetParameter(0,1)
      Fit_data.SetParameter(1,0)
      Fit_c_data.SetParameter(0,1)
      Fit_c_data.SetLineColor(ROOT.kBlack)
      #Fit_c_data.SetLineStyle(2)
      Fit_c_data.SetLineWidth(3)

      Fit_mc = ROOT.TF1('pol1','[0]+[1]*x', Bin_Cutoff, Bin_Max)
      Fit_c_mc = ROOT.TF1('pol0','[0]', Bin_Cutoff, Bin_Max)
      Fit_mc.SetParameter(0,1)
      Fit_mc.SetParameter(1,0)
      Fit_c_mc.SetParameter(0,1)
      Fit_c_mc.SetLineColor(ROOT.kGreen+3)
      #Fit_c_mc.SetLineStyle(2)
      Fit_c_mc.SetLineWidth(3)

      Fit_c_SF = ROOT.TF1(str(random.random()),'[0]', Bin_Cutoff, Bin_Max)
      #Fit_c_SF = ROOT.TF1('pol0','[0]', Bin_Cutoff, Bin_Max)
      Fit_c_SF.SetParameter(0,1)
      Fit_c_SF.SetLineColor(ROOT.kBlue+2)
      #Fit_c_SF.SetLineStyle(2)
      Fit_c_SF.SetLineWidth(3)


      if 'pt' in plotvar:
        #print ' ~~~~~~~~~ ', padc
        Hist_SF.Fit(Fit_c_SF, 'R Q') # order 0
        #print ' ~~~~~~~~~ ', padc, "WHY? - fixed by root 6!?" #TODO examine this!!!! TODO

        Hist_efficiency_data.Fit(Fit_data, 'R Q')   # order 1
        Hist_efficiency_data.Fit(Fit_c_data, 'R Q') # order 0

        Hist_efficiency_mc.Fit(Fit_mc, 'R Q')   # order 1
        Hist_efficiency_mc.Fit(Fit_c_mc, 'R Q') # order 0

        Fit_P0_data     = Fit_data.GetParameter(0)
        Fit_P0_Err_data = Fit_data.GetParError(0)

        Fit_P1_data     = Fit_data.GetParameter(1)
        Fit_P1_Err_data = Fit_data.GetParError(1)

        Fit_c_P0_data     = Fit_c_data.GetParameter(0)
        Fit_c_P0_Err_data = Fit_c_data.GetParError(0)

        Fit_P0_mc     = Fit_mc.GetParameter(0)
        Fit_P0_Err_mc = Fit_mc.GetParError(0)

        Fit_P1_mc     = Fit_mc.GetParameter(1)
        Fit_P1_Err_mc = Fit_mc.GetParError(1)

        Fit_c_P0_mc     = Fit_c_mc.GetParameter(0)
        Fit_c_P0_Err_mc = Fit_c_mc.GetParError(0)

        Fit_c_P0_SF     = Fit_c_SF.GetParameter(0)
        Fit_c_P0_Err_SF = Fit_c_SF.GetParError(0)

        results = [round(Fit_c_P0_mc,5), round(Fit_c_P0_Err_mc,5), round(Fit_c_P0_data,5), round(Fit_c_P0_Err_data,5), round(Fit_c_P0_SF,5), round(Fit_c_P0_Err_SF,5)]
      else:
        Hist_efficiency_mc.GetXaxis().SetTitle(plotvar)

      Hist_efficiency_mc.SetMinimum(0.0)
      Hist_efficiency_mc.SetMaximum(1.0)
      Hist_efficiency_mc.GetXaxis().SetRangeUser(Bin_Min,Bin_Max)

      # error here if pad disappears
      try:
        padc.cd()
      except:
        print 'stopping plots, returning results (pad disappeared)'
        return results # to be saved to spreadsheet

      #Hist_SF.Draw('E2') 
      Hist_efficiency_mc.Draw('AP E2') 
      Hist_efficiency_mc.Draw('SAME E') 
      Hist_efficiency_data.Draw('SAME E2')
      Hist_efficiency_data.Draw('SAME E')

      ROOT.ATLASLabel(0.15, 0.84, "Internal")

      LumiLabel= ROOT.TLatex()
      LumiLabel.SetTextFont(42)
      LumiLabel.SetTextSize(0.03)
      LumiLabel.SetNDC()
      LumiLabel.DrawLatex(0.15, 0.80,'L = ' + str(round(lumi,2))+ ' fb^{-1}')

      '''if year_selected == '2016':
        lumi_selected = str(round(lumi16,2))
      else:
        lumi_selected = str(round(lumi15,2))
      if 'pt' in plotvar:
        LumiLabel.DrawText(50, 0.91, year_selected + ' data, L=' + lumi_selected + '/fb')
      elif 'eta' in plotvar:
        LumiLabel.DrawText(-1.5, 0.91, year_selected + ' data, L=' + lumi_selected + '/fb')
      elif 'phi' in plotvar:
        LumiLabel.DrawText(-2.5, 0.91, year_selected + ' data, L=' + lumi_selected + '/fb')
      elif plotvar == 'mu':
        LumiLabel.DrawText(5, 0.91, year_selected + ' data, L=' + lumi_selected + '/fb')'''

      PlotLabel = ROOT.TText()
      PlotLabel.SetTextFont(42)
      PlotLabel.SetTextSize(0.03)
      #PlotLabel.SetTextAlign(12)

      if 'pt' in plotvar:
        #PlotLabel.DrawText(Label_xCoord, Label_yCoord, 'Straight Line Fit: '+ str(round(Fit_P0,3)) + ' +/- ' + str(round(Fit_P0_Err,3)))
        data_eff = Fit_c_P0_data
        data_eff_err = Fit_c_P0_Err_data
        mc_eff = Fit_c_P0_mc
        mc_eff_err = Fit_c_P0_Err_mc
        SF = Fit_c_P0_data / Fit_c_P0_mc
        SF_err = ( (data_eff_err / data_eff)**2  + (mc_eff_err / mc_eff)**2 )**0.5 * SF 
    

      if 'pt' in plotvar:
        PlotLabel.DrawText(70, 0.50, 'SF:')
        PlotLabel.DrawText(120, 0.50, 'ratio of fits:')
        PlotLabel.DrawText(120, 0.46, 'direct fit:')
        PlotLabel.DrawText(215, 0.50, str(round(SF,4)) + ' +/- ' + str(round(SF_err,4)))
        PlotLabel.DrawText(215, 0.46, str(round(Fit_c_P0_SF,4)) + ' +/- ' + str(round(Fit_c_P0_Err_SF,4)))

        PlotLabel.DrawText(70, 0.40,  'MC:')
        PlotLabel.DrawText(120, 0.40, 'fit offset:')
        PlotLabel.DrawText(120, 0.36, 'slope:')
        PlotLabel.DrawText(120, 0.32, 'const. fit: ')
        PlotLabel.DrawText(215, 0.40, str(round(Fit_P0_mc,4)) + ' +/- ' + str(round(Fit_P0_Err_mc,4)))
        PlotLabel.DrawText(215, 0.36, str(round(Fit_P1_mc,4)) + ' +/- ' + str(round(Fit_P1_Err_mc,4)))
        PlotLabel.DrawText(215, 0.32, str(round(Fit_c_P0_mc,4)) + ' +/- ' + str(round(Fit_c_P0_Err_mc,4)))

        PlotLabel.DrawText(70, 0.26, 'data:')
        PlotLabel.DrawText(120, 0.26, 'fit offset:')
        PlotLabel.DrawText(120, 0.22, 'slope:')
        PlotLabel.DrawText(120, 0.18, 'const. fit:')
        PlotLabel.DrawText(215, 0.26, str(round(Fit_P0_data,4)) + ' +/- ' + str(round(Fit_P0_Err_data,4)))
        PlotLabel.DrawText(215, 0.22, str(round(Fit_P1_data,4)) + ' +/- ' + str(round(Fit_P1_Err_data,4)))
        PlotLabel.DrawText(215, 0.18, str(round(Fit_c_P0_data,4)) + ' +/- ' + str(round(Fit_c_P0_Err_data,4)))
      
        # summary for spreadsheet
        print ' ~~~ '
        print ' ~~~ ', ProbeTrig, ' ~~~ ', Region, ' ~~~ ', year_selected
        print ' ~~~ ', str(round(Fit_c_P0_mc,5)) + ',' + str(round(Fit_c_P0_Err_mc,5)) + ','\
                     + str(round(Fit_c_P0_data,5)) + ',' + str(round(Fit_c_P0_Err_data,5)) + ','\
                     + str(round(Fit_c_P0_SF,5)) + ',' + str(round(Fit_c_P0_Err_SF,5))
        print ' ~~~ '
        #results = [round(Fit_c_P0_mc,5), round(Fit_c_P0_Err_mc,5), round(Fit_c_P0_data,5), round(Fit_c_P0_Err_data,5), round(Fit_c_P0_SF,5), round(Fit_c_P0_Err_SF,5)]

        #return results # to be saved to spreadsheet

      #leg = ROOT.TLegend(0.66, 0.71, 0.89, 0.89)
      if 'pt' in plotvar:
        leg = ROOT.TLegend(0.61, 0.06, 0.89, 0.29)
      else:
        leg = ROOT.TLegend(0.61, 0.16, 0.89, 0.21)
      leg.AddEntry(Hist_efficiency_mc,   'MC eff.', 'f')
      if 'pt' in plotvar:
        leg.AddEntry(Fit_c_mc,   'MC fit', 'l')
      leg.AddEntry(Hist_efficiency_data, 'data eff.', 'F')
      if 'pt' in plotvar:
        leg.AddEntry(Fit_c_data, 'data fit', 'l')
      if 'pt' in plotvar:
        leg.AddEntry(Hist_SF, 'SF', 'F')
        leg.AddEntry(Fit_c_SF, 'SF fit', 'l')

      leg.SetTextSize(0.03)

      leg.SetNColumns(2)

      leg.SetBorderSize(0)
      leg.SetFillColorAlpha(0,1)
      leg.Draw("SAME")

      if 'pt' in plotvar:
        can.cd()
        pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
        pad2.SetTopMargin(0.05)
        pad2.SetBottomMargin(0.25)
        pad2.SetGridx()
        #pad2.Modified() # suddenly
        #pad2.Update()   # needed?
        pad2.SetFillStyle(4000)
        pad2.SetFillColor(0)
        pad2.SetFrameFillStyle(4000)
        pad2.Draw()
        pad2.cd()
 
        Hist_SF.GetXaxis().SetTitle('muon p_{T} [GeV]')
        Hist_SF.GetYaxis().SetTitle('SF (data / MC eff.)')

        Hist_SF.GetYaxis().SetTitleOffset(0.5)

        Hist_SF.GetXaxis().SetTitleSize(0.1)
        Hist_SF.GetYaxis().SetTitleSize(0.1)

        Hist_SF.GetXaxis().SetLabelSize(0.1)
        Hist_SF.GetYaxis().SetLabelSize(0.1)

        Hist_SF.SetTitle('')

        Hist_SF.Draw('E2') 
        Hist_SF.Draw('SAME E') 

      can.cd()
      padc.Modified()
      if 'pt' in plotvar:
        pad2.Modified()
      can.Modified()

      if 'pt' in plotvar:
        plotvar = 'mu_pt'
      #can.SaveAs(plot_folder + 'TagTrigger_'+TagTrig+'_'+plotvar+'/'+ProbeTrig+'_'+Region+'_Pt.pdf')
      can.SaveAs(plot_folder + 'TagTrigger_'+TagTrig+'_'+plotvar+'/Pt_'+Region+'_'+ProbeTrig+'_Nominal.pdf')

      padc.Clear()
      if 'pt' in plotvar:
        pad2.Clear()
      can.Clear()

      emptyHist.Delete()
      Hist_pt_tagOnly_mc.Delete()
      Hist_pt_withProbe_mc.Delete()
      Hist_pt_tagOnly_data.Delete()
      Hist_pt_withProbe_data.Delete()
      Hist_efficiency_data.Delete()
      Hist_efficiency_mc.Delete()
      Hist_SF.Delete()

      if 'pt' in plotvar:
        return results # to be saved to spreadsheet
      else:
        return 0

    # ######################################################################
    # Eta-Phi trigger effiency map
    # ######################################################################
    if doEtaPhi:
      if not os.path.exists(plot_folder + 'TagTrigger_'+TagTrig+'_'+'2D'):
  	os.makedirs(plot_folder + 'TagTrigger_'+TagTrig+'_'+'2D')


      can = ROOT.TCanvas()
      CutString_tagOnly   = CombineCutStrings([CutString_tagOnly,   'mu_pt/1000>'+str(Bin_Cutoff)], '&&')
      CutString_withProbe = CombineCutStrings([CutString_withProbe, 'mu_pt/1000>'+str(Bin_Cutoff)], '&&')

      #eta 15, array('d', [-2.5, -2.0, -1.6, -1.3, -1, -0.7, -0.4, -0.1, 0.1, 0.4, 0.7, 1, 1.3, 1.6, 2.0, 2.5])
      #phi 17, array('d', [-3.16, -2.905, -2.59, -2.12, -1.805, -1.335, -1.02, -0.55, -0.235, 0.235, 0.55, 1.02, 1.335, 1.805, 2.12, 2.59, 2.905, 3.16])

      #empty2DHist           = ROOT.TH2D('empty2DHist',           '', 15, array('d', [-2.5, -2.0, -1.6, -1.3, -1, -0.7, -0.4, -0.1, 0.1, 0.4, 0.7, 1, 1.3, 1.6, 2.0, 2.5]),\  # old binning
      if Region == 'barrel':
        nbins_phi = 8
        empty2DHist           = ROOT.TH2D('empty2DHist',           '', 14,        array('d', [-1.05,-0.908,-0.791,-0.652,-0.476,-0.324,-0.132,0.0,\
                                                                                             +0.132,+0.324,+0.476,+0.652,+0.791,+0.908,+1.05]),\
                                                                       nbins_phi, array('d', [-3.15+(3.15*2/float(nbins_phi))*i for i in range(nbins_phi+1)]))
        Hist_etaphi_tagOnly   = ROOT.TH2D('Hist_etaphi_tagOnly',   '', 14,        array('d', [-1.05,-0.908,-0.791,-0.652,-0.476,-0.324,-0.132,0.0,\
                                                                                             +0.132,+0.324,+0.476,+0.652,+0.791,+0.908,+1.05]),\
                                                                       nbins_phi, array('d', [-3.15+(3.15*2/float(nbins_phi))*i for i in range(nbins_phi+1)]))
        Hist_etaphi_withProbe = ROOT.TH2D('Hist_etaphi_withProbe', '', 14,        array('d', [-1.05,-0.908,-0.791,-0.652,-0.476,-0.324,-0.132,0.0,\
                                                                                             +0.132,+0.324,+0.476,+0.652,+0.791,+0.908,+1.05]),\
                                                                       nbins_phi, array('d', [-3.15+(3.15*2/float(nbins_phi))*i for i in range(nbins_phi+1)]))
      elif Region == 'endcaps':
        nbins_phi = 12
        empty2DHist           = ROOT.TH2D('empty2DHist',           '', 15,        array('d', [-2.5,-2.4,-1.918,-1.623,-1.348,-1.2329,-1.1479,-1.05,+1.05,\
                                                                                              +1.1479,+1.2329,+1.348,+1.623,+1.918,+2.4,+2.5]),\
                                                                       nbins_phi, array('d', [-3.15+(3.15*2/float(nbins_phi))*i for i in range(nbins_phi+1)]))

        Hist_etaphi_tagOnly   = ROOT.TH2D('Hist_etaphi_tagOnly',   '', 15,        array('d', [-2.5,-2.4,-1.918,-1.623,-1.348,-1.2329,-1.1479,-1.05,+1.05,\
                                                                                              +1.1479,+1.2329,+1.348,+1.623,+1.918,+2.4,+2.5]),\
                                                                       nbins_phi, array('d', [-3.15+(3.15*2/float(nbins_phi))*i for i in range(nbins_phi+1)]))
        Hist_etaphi_withProbe = ROOT.TH2D('Hist_etaphi_withProbe', '', 15,        array('d', [-2.5,-2.4,-1.918,-1.623,-1.348,-1.2329,-1.1479,-1.05,+1.05,\
                                                                                              +1.1479,+1.2329,+1.348,+1.623,+1.918,+2.4,+2.5]),\
                                                                       nbins_phi, array('d', [-3.15+(3.15*2/float(nbins_phi))*i for i in range(nbins_phi+1)]))


      #empty2DHist           = ROOT.TH2D('empty2DHist',           '', NumBinsInEta, Eta_Min, Eta_Max, NumBinsInPhi, Phi_Min, Phi_Max)
      #Hist_etaphi_tagOnly   = ROOT.TH2D('Hist_etaphi_tagOnly',   '', NumBinsInEta, Eta_Min, Eta_Max, NumBinsInPhi, Phi_Min, Phi_Max)
      #Hist_etaphi_withProbe = ROOT.TH2D('Hist_etaphi_withProbe', '', NumBinsInEta, Eta_Min, Eta_Max, NumBinsInPhi, Phi_Min, Phi_Max)

      if inputRootFiles_mc == None:
        isData = True
        data_string = 'Data Eff.'
      else:
        isData = False
        data_string = 'MC Eff.'
      # Stich different pT slices together
      for rootFile in inputRootFiles:
        if isData:
	  if year_selected == '2015':
	    Hist_etaphi_tagOnly.Add( Hist_etaphi_tagOnly, GetHisto(rootFile, 'mu_phi:mu_eta', '(' + '1' + ')*(' + CutString_tagOnly   +')', empty2DHist, HF='2015') )
	    Hist_etaphi_withProbe.Add( Hist_etaphi_withProbe, GetHisto(rootFile, 'mu_phi:mu_eta', '(' + '1' + ')*(' + CutString_withProbe   +')', empty2DHist, HF='2015') )
	  if year_selected == '2016':
	    Hist_etaphi_tagOnly.Add( Hist_etaphi_tagOnly, GetHisto(rootFile, 'mu_phi:mu_eta', '(' + '1'+ ')*(' + CutString_tagOnly   +')', empty2DHist, HF='2016') )
	    Hist_etaphi_withProbe.Add( Hist_etaphi_withProbe, GetHisto(rootFile, 'mu_phi:mu_eta', '(' + '1' + ')*(' + CutString_withProbe   +')', empty2DHist, HF='2016') )
	else:
	  Hist_etaphi_tagOnly.Add( Hist_etaphi_tagOnly, GetHisto(rootFile, 'mu_phi:mu_eta', '(' + EventWeight + ')*(' + CutString_tagOnly   +')', empty2DHist) )
	  Hist_etaphi_withProbe.Add( Hist_etaphi_withProbe, GetHisto(rootFile, 'mu_phi:mu_eta', '(' + EventWeight + ')*(' + CutString_withProbe   +')', empty2DHist) )

      Hist_etaphi_withProbe.Divide(Hist_etaphi_tagOnly)

      unc_str = ''
      if doUncEtaPhi: unc_str += ' Unc.'

      Hist_etaphi_withProbe.SetTitle(ProbeTrig+' ('+Region+', p_{T}^{#mu}>'+str(Bin_Cutoff)+')' + ' ' + year_selected + ' ' + data_string + unc_str)
      Hist_etaphi_withProbe.GetXaxis().SetTitle('#eta')
      Hist_etaphi_withProbe.GetYaxis().SetTitle('#phi')

      if doUncEtaPhi:
        for ibin in range(1, Hist_etaphi_withProbe.GetNbinsX()+1):
          for jbin in range(1, Hist_etaphi_withProbe.GetNbinsY()+1):
            #print Hist_etaphi_withProbe.GetBinContent(ibin, jbin), Hist_etaphi_withProbe.GetBinError(ibin, jbin)
            Hist_etaphi_withProbe.SetBinContent(ibin, jbin, Hist_etaphi_withProbe.GetBinError(ibin, jbin))
            if Hist_etaphi_withProbe.GetBinError(ibin, jbin) > 1:
              Hist_etaphi_withProbe.SetBinContent(ibin, jbin, 1) # uncertainty cannot exceed 1!
              print 'tag', Hist_etaphi_tagOnly.GetBinContent(ibin, jbin)
              print 'error tag', Hist_etaphi_tagOnly.GetBinError(ibin, jbin)
              print 'ratio', Hist_etaphi_withProbe.GetBinContent(ibin, jbin)
              print 'error ratio', Hist_etaphi_withProbe.GetBinError(ibin, jbin)

      can.cd()

      unc_str = ''
      if doUncEtaPhi: unc_str += '_err'
     
      Hist_etaphi_withProbe.Draw('colz') #colz

      ROOT.ATLASLabel(0.11, 0.835, "Internal")
      '''latex = ROOT.TLatex()
      latex.SetNDC()
      latex.SetTextFont(72)
      #latex.SetTextAlign(13)
      latex.SetTextSize(0.025)
      latex.DrawLatex(0.05, 0.85, "ATLAS")

      delx = 0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw())
      latex.SetTextFont(42)
      latex.DrawLatex(0.05+delx, 0.05, "Internal")'''

      if isData:
	  can.Print(plot_folder + 'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_Nominal_data' + unc_str + '.pdf')
	  #can.Print(plot_folder + 'TagTrigger_'+TagTrig+'_2D/'+ProbeTrig+'_'+Region+'_EtaPhi_data.pdf')
      else:
	  can.Print(plot_folder + 'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_Nominal_mc' + unc_str + '.pdf')
	  #can.Print(plot_folder + 'TagTrigger_'+TagTrig+'_2D/'+ProbeTrig+'_'+Region+'_EtaPhi.pdf')


# Start by merging sub-files to get proper normalization N
if doMerging:
    for sample in mcSamples:
        print sample+'output.root'
        if not(os.path.isfile(sample+'output.root')):
            print sample 
            os.system('hadd -f '+sample+'output.root '+sample+'*')
    for sample in dataSamples:
        print sample
        if not(os.path.isfile(sample+'output.root')):
            os.system('hadd -f '+sample+'output.root '+sample+'*')
        #os.system('hadd -f '+sample+'output.root '+sample+'*')


#CreateCutFlow(mcSamples)
#CreateCutFlow(dataSamples)
if raw_input("produce validation plots? (y/n): ") == 'y':
  CreateValidationPlots(mcSamples, dataSamples, EventWeight, lumi)
#  print 'exiting...'
  raise SystemExit

#ws = sts.getws() # get spreadsheet

for ProbeTrig in ProbeTriggers:
  #for Region in ['barrel', 'endcaps']:
  for Region in ['endcaps', 'barrel']:
#      results = CreateEfficiencyHistograms(mcSamples, dataSamples, trigger, ProbeTrig, Region, EventWeight, plotvar='mu')
#      results = CreateEfficiencyHistograms(mcSamples, dataSamples, trigger, ProbeTrig, Region, EventWeight, plotvar='mu_eta')
#      results = CreateEfficiencyHistograms(mcSamples, dataSamples, trigger, ProbeTrig, Region, EventWeight, plotvar='mu_phi')

      optimize_binning = False#True
      if optimize_binning:
        pass  # find binning code at alheld@atlas-tier3-c9.triumf.ca:/global/alheld/servicetask/2016/07_AsymSF.py

      else:
        custom_binning = []
        results = CreateEfficiencyHistograms(mcSamples, dataSamples, trigger, ProbeTrig, Region, EventWeight, plotvar='mu_pt', custom_binning = [])              # old binning setup 

      #raise SystemExit
         
      if trigger in trigger_2015:
        year = '2015' 
      elif trigger in trigger_2016:
        year = '2016'
      else:
        print 'invalid option for trigger year'
        raise SystemExit
      
      #if results != 0: 
      #  sts.update_sheet(ws, results, ProbeTrig, Region, year, systematics_name_spreadsheet)

      #CreateEfficiencyHistograms(mcSamples, None,        trigger, ProbeTrig, Region, EventWeight, doEtaPhi=True)
      #CreateEfficiencyHistograms(None,      dataSamples, trigger, ProbeTrig, Region, EventWeight, doEtaPhi=True)
