### description of files for main workflow:
* reference for these: ```/data/alheld/servicetask/AnalysisTop-2.4.25``` on flashy
* files in this directory are being updated
* start with ```01_Preprocess.py``` to merge files and append normalization weights
  * normalization is wrong when using first bin in cutflow histogram and sample is skimmed (p3260), ```getN_fromcutflow()``` is not recommended (by default, script uses ```getN()```)
* produce plots via ```python 02_Analysis.py path/to/config.json```, see ```config.json``` for configuration options

### various details:
* current x-sec file: [TopDataPreparation-XSection-MC15-13TeV](http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC15-13TeV.data)
* how to update config between data/MC and SF plots:
   * ```MCStackOrder``` to ```allMC```
   * ```DownloadFolder``` to ```../2_download``` 
   * ```DefaultCuts``` to ```(1==1)``` 
   * remove trigger efficiency from ```Weights``` 
   * switch ```mu_pt``` bins
* how to switch between topologies:
   * update ```DefaultCuts``` 
* scan to remove large event weights: ```nominal->Scan("eventNumber:mcChannelNumber:mu_pt[0]/1000.0:50000*weight_mc*weight_pileup*weight_norm*weight_jvt*weight_bTagSF_MV2c10_77*weight_leptonSF","(mu_pt[0]/1000.0>175)*(50000*weight_mc*weight_pileup*weight_norm*weight_jvt*weight_bTagSF_MV2c10_77*weight_leptonSF<-25)")```

### naming scheme of output files:
Root files containing efficiencies/SFs are named according to the following scheme:

```
trigger.region.year.topology.mu_qual.mu_iso.other_reqs.root
```

where:

* ```trigger``` describes the trigger used, can be ```mu26_ivarmedium``` or ```HLT_mu50```; ```HLT_mu26_ivarmedium_OR_HLT_mu50``` is used for a logical or between both
* ```region``` is the detector region, ```barrel``` or ```endcaps```
* ```year``` describes the data-taking year, ```2016```,```2017```, or ```2018```
* ```topology``` is ```ttbar``` or ```wjets```, specifying the selection requirements
* ```mu_qual``` refers to the muon quality working point, used here are ```medium_wp``` (nominal) and ```highpt_wp``` (systematic variation)
* ```mu_iso``` refers to the offline isolation requirement, ```no_iso``` (nominal), ```FCTight``` or ```FCTTO```
* ```other_reqs``` denotes variations on the nominal cuts. Variations (other than ```nominal```) are only calculated for ```noIso```, and can take the following values:
   * ```nominal``` for no variation
   * ```btag_var``` uses the 70% mv2c10 working point (nominal is 77%)
   * ```met_var``` denotes a 150 GeV offline MET cut (instead of nominal 200 GeV)
   * ```jetpt_var``` requires a jet p<sub>T</sub> of 30 GeV for all jets (nominal is 25 GeV)
