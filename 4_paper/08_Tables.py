import math
import ROOT
from subprocess import call

out_dir = '/Users/Sebastien/Dropbox/Courses/ServiceWork/PaperPlots/'

triggers = ['HLT_mu26_ivarmedium_OR_HLT_mu50']
regions = ['barrel', 'endcaps']
selections = {
    #'Zmumu'     : '/Users/sebastien/Downloads/MuTPaper/Zmumu/Zmumu.root',
    'ttbar' : '/Users/sebastien/Downloads/MuTPaper/HighPt/ttbar.root',
    'wjets' : '/Users/sebastien/Downloads/MuTPaper/HighPt/wjets.root',
}
systematics = {
    #'Zmumu' : ['PU','TP','Zwindow','Charge','ImpactParams','FCTTO','FCTight','var_pt'],
    'ttbar' : ['met_var','btag_var','jetpt_var','highpt_wp','iso'],
    'wjets' : ['met_var','btag_var','jetpt_var','highpt_wp','iso'],
}


debug = False

def print_summary_table(year):
    print('\\text{Summary ' + year + ' SF table}\\\\')

    print('\\begin{table}[H]')
    print('\\centering')
    print('\\resizebox{\\textwidth}{!}{')
    print('\\begin{tabular}{cccccc}')
    print('\\toprule')
    print('Trigger & Selection & Region & SF & (stat.) & (syst.) \\\\')
    print('\midrule')

    # Get summary file produced earlier
    fname = out_dir + 'systematics_summary_' + year + '.txt'
    try:
        with open(fname) as f:
            for l in f:
                if len(l) < 6:
                    print('Missing information! Faulty line: ' + l)
                    continue
                buff = l.split()
                trig = buff[0].replace('_','\_')
                reg = buff[1]
                sel = buff[2]
                sf = float(buff[3])
                stat = float(buff[4])
                sys = float(buff[5])
                err = math.sqrt(stat*stat + sys*sys)

                table_line = trig + ' & ' + sel + ' & ' + reg + ' & '
                table_line += format(sf, '.2f') + '\% $\pm$ '
                table_line += format(err, '.2f') + '\% & ('
                table_line += format(stat, '.2f') + '\%) & ('
                table_line += format(sys, '.2f') + '\%)\\\\'
                print(table_line)
    except IOError:
        print('File ' + fname + ' not found! Just printed blank table...')
        
    print('\\bottomrule')
    print('\end{tabular}}\\\\')
    print('\caption{Summary of systematic uncertainties in '+year+'.}')
    print('\label{tab:TrigSys'+year+'}')
    print('\end{table}')
    
def print_table(trig, sel, year):
    print('\\text{Table for ' + year + ' ' + trig.replace('_','\_') + ' (' + sel + ')}\\\\')

    print('\\begin{table}[H]')
    print('\\centering')
    print('\\resizebox{\\textwidth}{!}{')
    print('\\begin{tabular}{cccccc}')
    print('\\toprule')
    print('Region & Variation & MC eff. & Data eff. & SF & (Syst-Nom)/Nom \\\\')
    print('\midrule')
    
    for r in regions:
        # Open histograms file
        nom_name = trig + '.' + r + '.' + year + '.' + sel + '.medium_wp.no_iso.nominal.root'
        if debug:
            print('Opening histograms file: ' + selections[sel])
            print('Nominal histogram: ' + nom_name)
        try:
            f = ROOT.TFile.Open(selections[sel], 'READ')

            h_sf_nom = f.Get(nom_name + '.SF_fit')
            sf_nom = 100.0*h_sf_nom.GetParameter(0)
            stat_err_nom = 100.0*h_sf_nom.GetParError(0)

            h_mc_nom = f.Get(nom_name + '.eff_mc_fit')
            eff_mc_nom = 100.0*h_mc_nom.GetParameter(0)
            stat_err_mc_nom = 100.0*h_mc_nom.GetParError(0)

            h_data_nom = f.Get(nom_name + '.eff_data_fit')
            eff_data_nom = 100.0*h_data_nom.GetParameter(0)
            stat_err_data_nom = 100.0*h_data_nom.GetParError(0)

            if debug:
                print('Nominal SF: ' + str(sf_nom) + ' +/- ' + str(stat_err_nom))
                
            sys_err = 0.0
        except ReferenceError:
            print('Could not open nominal file ' + selections[sel] + '!')
            return

        table_line = '\multirow{' + str(len(systematics[sel])+1) + '}{*}{' + r + '}' + ' & Nominal & '
        table_line += format(eff_mc_nom, '.2f') + '\% $\pm$ ' + format(stat_err_mc_nom, '.2f') + '\% & '
        table_line += format(eff_data_nom, '.2f') + '\% $\pm$ ' + format(stat_err_data_nom, '.2f') + '\% & '
        table_line += format(sf_nom, '.2f') + '\% $\pm$ ' + format(stat_err_nom, '.2f') + '\% & '
        table_line += ' N/A \\\\'
        print(table_line)
        
        # Get systematics histograms
        for sys in systematics[sel]:
            var_name = trig + '.' + r + '.' + year + '.' + sel + '.medium_wp.no_iso.' + sys + '.root'
            if sys == 'highpt_wp':
                var_name = nom_name.replace('medium_wp','highpt_wp')
            if sys == 'iso':
                # For isolation envelop, compare FCTTO and FCTight and take the largest
                var_name = nom_name.replace('no_iso','FCTTO')
                h_sf_FCTTO = f.Get(var_name + '.SF_fit')
                sf_FCTTO = 100.0*h_sf_FCTTO.GetParameter(0)
                diff_FCTTO = abs(sf_FCTTO - sf_nom)

                var_name = nom_name.replace('no_iso','FCTight')
                h_sf_FCTight = f.Get(var_name + '.SF_fit')
                sf_FCTight = 100.0*h_sf_FCTight.GetParameter(0)
                diff_FCTight = abs(sf_FCTight - sf_nom)

                if diff_FCTight > diff_FCTTO:
                    var_name = nom_name.replace('no_iso','FCTight')
                else:
                    var_name = nom_name.replace('no_iso','FCTTO')


            try:
                if debug:
                    print('Systematic variation histogram: ' + var_name)

                h_sf = f.Get(var_name + '.SF_fit')
                sf = 100.0*h_sf.GetParameter(0)
                stat_err = 100.0*h_sf.GetParError(0)

                h_mc = f.Get(var_name + '.eff_mc_fit')
                eff_mc = 100.0*h_mc.GetParameter(0)
                stat_err_mc = 100.0*h_mc.GetParError(0)

                h_data = f.Get(var_name + '.eff_data_fit')
                eff_data = 100.0*h_data.GetParameter(0)
                stat_err_data = 100.0*h_data.GetParError(0)

                if debug:
                    print('Systematically varied SF (' + sys + '): ' + str(sf) + ' +/- ' + str(stat_err))
                rel_diff = 100*(sf - sf_nom)/sf_nom
                sys_err += rel_diff**2
            except ReferenceError:
                print('Could not find systematic variation histogram ' + var_name + '!')

            table_line = '& ' + sys.replace('_','\_') + ' & '
            table_line += format(eff_mc, '.2f') + '\% $\pm$ ' + format(stat_err_mc, '.2f') + '\% & '
            table_line += format(eff_data, '.2f') + '\% $\pm$ ' + format(stat_err_data, '.2f') + '\% & '
            table_line += format(sf, '.2f') + '\% $\pm$ ' + format(stat_err, '.2f') + '\% & '
            table_line += format(rel_diff, '.2f') + '\% \\\\'
            print(table_line)
            
        sys_err = math.sqrt(sys_err)
        summary_line = trig + ' '  + r + ' ' + sel + ' ' + str(sf_nom) + ' ' + str(stat_err) + ' ' + str(sys_err) + '\n'
        if debug:
            print(summary_line)
        with open(out_dir + 'systematics_summary_' + year + '.txt','a') as outfile:
            outfile.write(summary_line)
    print('\\bottomrule')
    print('\end{tabular}}\\\\')
    print('\caption{Summary of systematic uncertainties for the '+trig.replace('_','\_')+' trigger in '+year+' using the '+sel+' selection.}')
    print('\label{tab:TrigSys'+year+sel+'}')
    print('\end{table}')

if __name__ == "__main__":
    for y in ['2016','2017','2018']:
        call('rm ' + out_dir + 'systematics_summary_' + y + '.txt', shell=True)
        for t in triggers:
            for s in selections:
                print_table(t, s, y)
        print_summary_table(y)


