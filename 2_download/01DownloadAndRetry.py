import getpass
import os
import json

# new features of pbook from DAST "Query panda servers without using pbook" mail:
# from pandatools.PBookCore import PBookCore
# pbook = PBookCore()
# Sync the local database
# pbook.sync()
# Get the list of all your jobs
# job_list = pbook.getLocalJobList() # Going by memory - the actual method name might be different
# You can then use standard python methods to look at whichever jobs you like. Similarly you can use pbook.retry(jobID), pbook.kill(jobID), etc to send kill or retry commands.


# ###############################
# configure the script here
# ###############################
suffix          = '59_'             # this corresponds to the suffix used in the grid submission script
username        = getpass.getuser() # manual overwrite needed if username on current machine != grid user name
OutputFilename  = "output.root"     # see cuts file used, "METADATA" for PRW
UpdateJSON      = True              # update json file every time if True (recommended)
SaveAllInOneDir = False             # save everything into the same output directory
OutDirName      = 'Files'           # save files into this directory
min_fraction    = 0.25
# ###############################

broken_pairs = [[16734701, 16768476],
                [16734479, 16768494],
               ]

class gridjob():
  def __init__(self, j):
    self.name     = j['taskname'][0:-1] + '_' + OutputFilename
    self.dsid     = self.name.split('.')[2]
    if not SaveAllInOneDir:
      self.category = self.name.split('-')[-1][0:-(len(OutputFilename)+1)]
    else:
      self.category = ''
    self.status   = j['superstatus']
    self.task_id  = j['jeditaskid']    # jedi task id
    self.jobset   = j['reqid']
    self.n_failed = j['dsinfo']['nfilesfailed']  # amount of failed files
    self.fraction_done  = j['dsinfo']['pctfinished'] / 100.0


def get_bigpanda_link(username, suffix, status="", json=True):
  """
  status: 'done' (safe to download), 'finished' (retry job), 'broken' (find out what is wrong)
  if status is "", no status requirement is made
  """
  link = 'https://bigpanda.cern.ch/tasks/?taskname=user.'
  link += username + '*' + suffix + '*'
  link += "&display_limit=5000&limit=5000&days=100"
  if status != "":
    link += '&superstatus=' + status
  if json:
    link += '&json'
  return link


def download_json(username, suffix):
  """
  download json file containing job details from bigpanda
  returns json file name
  """
  json_file_name = "jobs.json"
  link = get_bigpanda_link(username, suffix, status="", json=True)
  download_command = "wget --no-check-certificate -O " + json_file_name +\
                     " \"" + link + "\""
  print 'executing:', download_command
  os.system(download_command)
  return json_file_name


def create_job_list(overwrite=True):
  """
  create a list of gridjob() instances
  if overwrite==False, do not re-download the json file
  """
  if overwrite:
    fname = download_json(username, suffix) # get json object
  else:
    fname = "jobs.json"

  with open(fname) as f:
    job_info = json.load(f)
  job_list = []
  for j in job_info:
    job_list.append(gridjob(j)) # append list by instances of gridjob objects

  for j in job_list:
    print '  - found:', j.category, j.dsid, j.status   # print list of jobs found

  return job_list


def save_downloadable_jobs(job_list):
  """
  save list of grid jobs with status "done"
  that can then be downloaded via a separate script
  """
  job_categories = list(set([j.category for j in job_list]))

  # loop over different categories, where categories are defined by
  # different entries via TopExamples.grid.Add() in the grid submission
  for current_job_cat in job_categories:

    dir_path = OutDirName + '/' + current_job_cat
    # create directory to write in if it does not exist yet
    if not os.path.exists(dir_path):
      os.makedirs(dir_path)

    # write list of jobs with status "done" to file in directory
    output_filename = (dir_path + "/" + "files.txt").encode('utf8')
    #output_filename = output_filename.encode('utf8')

    # save jobs with status "done", or those with a fraction > "min_fraction" of finished sub-jobs
    jobs_to_write = [j.name for j in job_list if
                     (j.category == current_job_cat) and
                     ((j.status == 'done') or (j.fraction_done >= min_fraction))]

    print '  - writing list of', len(jobs_to_write), 'jobs to', output_filename

    with open (output_filename, 'w') as f:
      for job in jobs_to_write:
        f.write(job + '\n')


def retry_jobs(job_list):
  """
  retry all jobs via pbook if some files have failed
  and job is in status "running" or "finished"
  """

  # find all jobs with >0 files, running or finished (and not broken)
  jobs_to_retry = [j.task_id for j in job_list if
                   ((j.n_failed > 0) or (j.status == 'finished'))
                   and (j.status != 'broken')]

  print '  - found', len(jobs_to_retry), 'jobs to retry'

  for j in jobs_to_retry:
    os.system('pbook -c \"retry(' + str(j) + ')\"')


def list_broken_jobs(job_list):
  """
  list broken jobs (which need to be resubmitted)
  """
  # find all broken jobs
  broken_jobs = [j for j in job_list if (j.status == 'broken') and
                 ('59_med_FCTight_v0' not in j.name) and ('59_med_FCTTO_v0' not in j.name)]

  print '  - found', len(broken_jobs), 'broken jobs'

  for j in broken_jobs:
    tag = j.name.split('.')[-2].split('-')[0]
    print 'id:', j.task_id, ' category:', j.category, ' dsid:', j.dsid,\
          'tag:', tag, ' link: https://bigpanda.cern.ch/task/' + str(j.task_id)

  print ""
  for p in broken_pairs:
    print 'https://bigpanda.cern.ch/task/' + str(p[0]), 'https://bigpanda.cern.ch/task/' + str(p[1])


def sync_jobs():
  os.system('pbook -c \"sync()\"')


def kill_jobs(job_list):
  # find all jobs with >0 files, running or finished (and not broken)
  jobs_to_kill = [j.task_id for j in job_list if
                  (('data' not in j.name) and ('FCTight_v0' in j.name) and (j.status != 'broken' and j.status != 'aborted'))]
  print '  - found', len(jobs_to_kill), 'jobs to kill'

  for x in [j for j in job_list if (('data' not in j.name) and ('FCTight_v0' in j.name) and (j.status != 'broken' and j.status != 'aborted'))]:
    print x.name, x.status, x.task_id

  raw_input("go?")

  for j in jobs_to_kill:
    os.system('pbook -c \"kill(' + str(j) + ')\"')


if __name__ == '__main__':
  print "# to use this script:"
  print "# make sure to setup rucio and panda (lsetup)"
  print "# and verify that a valid proxy is configured\n"

  sync_jobs()

  job_list = create_job_list(overwrite=UpdateJSON) # obtain list of grid jobs

  list_broken_jobs(job_list)

  print "1 - retry incomplete jobs"
  print "2 - create list of files to download"
  print "3 - kill jobs"
  decision = raw_input("select option (1,2,3): ")

  if decision == "1":
    retry_jobs(job_list)
  elif decision == "2":
    save_downloadable_jobs(job_list)
  elif decision == "3":
    kill_jobs(job_list)
  else:
    print "option not recognized, exiting"
